// Chart forming by string concating
document.addEventListener('DOMContentLoaded', function () {

});

Object.prototype.getOwnProperties = function () {
    let result = [];
    for (let name in this) {
        if (this.hasOwnProperty(name)) {
            result.push(name);
        }
    }
    return result;
}

Document.prototype.createSvgElement = function (qualifiedName) {
    return this.createElementNS("http://www.w3.org/2000/svg", qualifiedName);
}

HTMLElement.prototype.removeChilds = function () {
    while (this.firstChild) {
        this.removeChild(this.firstChild);
    }
}

class Chart2 extends Chart {
    constructor(elementId, chartData) {
        super(elementId, chartData);
    }

    draw() {
        this.htmlElement.removeChilds();
        this.htmlElement.insertAdjacentHTML("afterbegin", this.getSvgElement())
    }

    getSvgElement() {
        return "<svg width='" + this.htmlElement.clientWidth + "' height='" + this.htmlElement.clientHeight + "' viewBox='" + this.xAxisMinValue + " " + this.yAxisMinValue + " " + (this.xAxisMaxValue - this.xAxisMinValue) + " " + (this.yAxisMaxValue - this.yAxisMinValue) + "' preserveAspectRatio='none'>"
            + this.getRectElement()
            + this.getSvgLineElements()
            + "</svg>";
    }

    getRectElement() {
        return "<rect width='" + this.htmlElement.clientWidth + "' height='" + this.htmlElement.clientHeight + "' fill='" + this.getBgColor() + "'></rect>";
    }

    getSvgLineElements() {
        var result = "";
        for (var i = 0; i < this.linesNames.length; i++) {
            result += this.getSvgLineElement(this.linesNames[i]);
        }
        return result;
    }

    getSvgLineElement(lineName) {
        let result = "<path fill='none' stroke='" + this.getLineColor(lineName) + "' stroke-width='" + this.getLineStrokeWidth() + "' d='" + this.getSvgLinePathD(lineName) + "'></path>";
        return result;
    }
}
