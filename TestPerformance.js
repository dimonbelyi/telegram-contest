document.addEventListener('DOMContentLoaded', function () {
    let start = performance.now();
    let el = document.getElementById("test1");
    for (var i = 0; i < 20000; i++) {
        let str = "<svg width='" + 1 + "' height='" + 1 + "' viewBox='" + 0 + " " + 0 + " " + (1) + " " + (1) + "' preserveAspectRatio='none'>"
            + "<rect width='" + 1 + "' height='" + 1 + "' fill='" + "red" + "'></rect>"
            + "</svg>";
        el.insertAdjacentHTML("beforeend", str);
    }
    let end = performance.now();
    console.log("insert html string = " + (end - start) + " ms");

    start = performance.now();
    el = document.getElementById("test2");
    for (var i = 0; i < 20000; i++) {
        let svgEl = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svgEl.setAttribute("width", "1");
        svgEl.setAttribute("height", "1");
        svgEl.setAttribute("preserveAspectRatio", "none ");
        svgEl.setAttribute("viewBox", "" + 0 + " " + 0 + " " + (1) + " " + (1));
        let rectEl = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        rectEl.setAttribute("width", "1");
        rectEl.setAttribute("height", "1");
        rectEl.setAttribute("fill", "red");
        svgEl.appendChild(rectEl);
        el.appendChild(svgEl);
    }
    end = performance.now();
    console.log("insert html element = " + (end - start) + " ms");
});