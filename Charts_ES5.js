"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MonthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var DaysNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
document.addEventListener('DOMContentLoaded', function () {
    var chart1 = new ChartGroup("chart1", chartsData[0], {
        extendAxisYToZero: true
    });
    chart1.draw();
    var chart2 = new ChartGroup("chart2", chartsData[1]);
    chart2.draw();
    var chart3 = new ChartGroup("chart3", chartsData[2]);
    chart3.draw();
    var chart4 = new ChartGroup("chart4", chartsData[3]);
    chart4.draw();
    var chart5 = new ChartGroup("chart5", chartsData[4]);
    chart5.draw();
});

Object.prototype.getOwnProperties = function () {
    var result = [];

    for (var name in this) {
        if (this.hasOwnProperty(name)) {
            result.push(name);
        }
    }

    return result;
};

Document.prototype.createSvgElement = function (qualifiedName) {
    return this.createElementNS("http://www.w3.org/2000/svg", qualifiedName);
};

Document.prototype.addCssStyle = function (cssStyle) {
    if (typeof cssStyle != "string") {
        throw "cssStyle must be string.";
    }

    var head = this.head || this.getElementsByTagName('head')[0];
    var style = this.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';

    if (style.styleSheet) {
        // This is required for IE8 and below.
        style.styleSheet.cssText = cssStyle;
    } else {
        style.appendChild(document.createTextNode(cssStyle));
    }
};

HTMLElement.prototype.removeChilds = function () {
    while (this.firstChild) {
        this.removeChild(this.firstChild);
    }
};

SVGElement.prototype.removeChilds = function () {
    while (this.firstChild) {
        this.removeChild(this.firstChild);
    }
};

Date.prototype.toShortString = function () {
    return MonthNames[this.getMonth()] + " " + this.getDate();
};

Date.prototype.toShortStringWithDayOfWeek = function () {
    return DaysNames[this.getDay()] + ", " + MonthNames[this.getMonth()] + " " + this.getDate();
};

var BaseChart =
    /*#__PURE__*/
    function () {
        function BaseChart(rootElementId, initialChartData, chartOptions) {
            _classCallCheck(this, BaseChart);

            _defineProperty(this, "defaultChartAxisLabelsHeight", 20);

            _defineProperty(this, "animateFramesCount", 7);

            _defineProperty(this, "defaultXAxisLabelAbsoluteWidth", 60);

            _defineProperty(this, "showingDataTooltipWidth", 150);

            _defineProperty(this, "tooltipCircleRadius", 6);

            _defineProperty(this, "initialChartData", {});

            _defineProperty(this, "options", new BaseChartOptions());

            _defineProperty(this, "rootElementId", "");

            _defineProperty(this, "rootElement", document.createElement("div"));

            _defineProperty(this, "absoluteXRange", new ChartRange(0, 0));

            _defineProperty(this, "absoluteYRange", new ChartRange(0, 0));

            _defineProperty(this, "chartMaxXRange", new ChartRange(0, 0));

            _defineProperty(this, "chartMaxYRange", new ChartRange(0, 0));

            _defineProperty(this, "displayChartXRange", new ChartRange(0, 0));

            _defineProperty(this, "displayChartYRange", new ChartRange(0, 0));

            _defineProperty(this, "displayChartXRangePrev", new ChartRange(0, 0));

            _defineProperty(this, "displayChartYRangePrev", new ChartRange(0, 0));

            _defineProperty(this, "displayChartXIndexesRange", new ChartRange(0, 0));

            _defineProperty(this, "displayChartXIndexesRangePrev", new ChartRange(0, 0));

            _defineProperty(this, "xAxisName", "");

            _defineProperty(this, "lines", []);

            _defineProperty(this, "sections", new BaseChartDOMElements());

            _defineProperty(this, "redrawPromise", Promise.resolve());

            _defineProperty(this, "updateChartRequests", []);

            _defineProperty(this, "showingDataTooltipIndex", -1);

            this.rootElementId = rootElementId;
            this.rootElement = document.getElementById(this.rootElementId);
            this.initialChartData = initialChartData;
            this.mergeOptions(chartOptions);
            this.xAxisName = this.getXAxisName();
            this.lines = this.getLines();
            this.absoluteXRange = new ChartRange(0, this.options.width);
            this.absoluteYRange = new ChartRange(this.options.showAxisX ? this.defaultChartAxisLabelsHeight : 0, this.options.showAxisY ? this.options.height - this.defaultChartAxisLabelsHeight : this.options.height);
            this.calculateMaxChartXAxis();
            this.redrawPromise = Promise.resolve();
            this.updateChartRequests = [];
        }

        _createClass(BaseChart, [{
            key: "setDisplayingXAxisRange",
            value: function setDisplayingXAxisRange(newRelativeRange) {
                // newRelativeRange - range start and range end are from 0 to 1;
                this.updateChartRequests.push({
                    type: "xRange",
                    newRelativeRange: newRelativeRange
                });
                this.executeRequests();
            }
        }, {
            key: "setLineVisisble",
            value: function setLineVisisble(lineName, visible) {
                this.updateChartRequests.push({
                    type: "lineVisible",
                    lineName: lineName,
                    visible: visible
                });
                this.executeRequests();
            }
        }, {
            key: "executeRequests",
            value: function executeRequests() {
                var self = this;
                self.redrawPromise = self.redrawPromise.then(function () {
                    if (self.updateChartRequests.length == 0) {
                        return;
                    }

                    var newRelativeRange = null;
                    var newLinesVisibilities = [];

                    for (var i = 0; i < self.updateChartRequests.length; i++) {
                        var request = self.updateChartRequests[i];

                        if (request.type == "xRange") {
                            newRelativeRange = request.newRelativeRange;
                        } else if (request.type == "lineVisible") {
                            newLinesVisibilities.push({
                                lineName: request.lineName,
                                visible: request.visible
                            });
                        }
                    }

                    var lineVisibilityPromise = Promise.resolve();

                    if (newLinesVisibilities.length > 0) {
                        lineVisibilityPromise = self.executeSetLineVisisbleRequests(newLinesVisibilities);
                    }

                    var newRangePromise = Promise.resolve();

                    if (newRelativeRange != null) {
                        newRangePromise = self.executeChangeXAxisRangeRequest(newRelativeRange);
                    }

                    self.redrawPromise = Promise.all([lineVisibilityPromise, newRangePromise]);
                    self.updateChartRequests = [];
                });
            }
        }, {
            key: "executeChangeXAxisRangeRequest",
            value: function executeChangeXAxisRangeRequest(newRelativeRange) {
                var self = this;
                var newStart = self.chartMaxXRange.start + newRelativeRange.start * self.chartMaxXRange.getDifference();
                var newEnd = self.chartMaxXRange.start + newRelativeRange.end * self.chartMaxXRange.getDifference();
                self.displayChartXRangePrev = self.displayChartXRange.clone();
                self.displayChartXRange = new ChartRange(newStart, newEnd);
                self.calculateDisplayChartXDataIndexes();
                return self.redraw();
            }
        }, {
            key: "executeSetLineVisisbleRequests",
            value: function executeSetLineVisisbleRequests(linesVisibilities) {
                this.displayChartXRangePrev = this.displayChartXRange.clone();
                var needRedraw = false;

                for (var i = 0; i < linesVisibilities.length; i++) {
                    for (var j = 0; j < this.lines.length; j++) {
                        if (this.lines[j].name == linesVisibilities[i].lineName) {
                            this.lines[j].visible = linesVisibilities[i].visible;
                            needRedraw = true;
                            break;
                        }
                    }
                }

                if (needRedraw) {
                    return this.redraw();
                } else {
                    return Promise.resolve();
                }
            }
        }, {
            key: "mergeOptions",
            value: function mergeOptions(opt) {
                if (opt && opt.mode) {
                    this.options.mode = opt.mode;
                }

                if (opt && typeof opt.showAxisX == "boolean") {
                    this.options.showAxisX = opt.showAxisX;
                }

                if (opt && typeof opt.showAxisY == "boolean") {
                    this.options.showAxisY = opt.showAxisY;
                }

                if (opt && opt.yAxisLinesCount) {
                    this.options.yAxisLinesCount = opt.yAxisLinesCount;
                }

                if (opt && opt.xAxisLabelsCount) {
                    this.options.xAxisLabelsCount = opt.xAxisLabelsCount;
                }

                var maxXLabelsCount = Math.trunc(opt.width / this.defaultXAxisLabelAbsoluteWidth);

                if (this.options.xAxisLabelsCount > maxXLabelsCount) {
                    this.options.xAxisLabelsCount = maxXLabelsCount;
                }

                if (opt && typeof opt.extendAxisYToZero == "boolean") {
                    this.options.extendAxisYToZero = opt.extendAxisYToZero;
                }

                if (opt && opt.height) {
                    this.options.height = opt.height;
                }

                if (opt && opt.width) {
                    this.options.width = opt.width;
                }

                if (opt && typeof opt.showDataOnHover == "boolean") {
                    this.options.showDataOnHover = opt.showDataOnHover;
                }
            }
        }, {
            key: "redraw",
            value: function redraw() {
                var promises = [];
                this.calculateMaxChartYAxis();

                if (this.options.showAxisY) {
                    var promise = this.redrawYAxis();
                    promises.push(promise);
                }

                if (this.options.showAxisX) {
                    var _promise = this.redrawXAxis();

                    promises.push(_promise);
                }

                var promise3 = this.redrawSvgLines();
                promises.push(promise3);
                return Promise.all(promises);
            }
        }, {
            key: "calculateMaxChartXAxis",
            value: function calculateMaxChartXAxis() {
                this.chartMaxXRange = new ChartRange(this.getXAxisMinValue(), this.getXAxisMaxValue());
                this.displayChartXRange = this.chartMaxXRange.clone();
                this.calculateDisplayChartXDataIndexes();
                this.displayChartXIndexesRangePrev = this.displayChartXIndexesRange.clone();
                this.calculateMaxChartYAxis();
            }
        }, {
            key: "calculateDisplayChartXDataIndexes",
            value: function calculateDisplayChartXDataIndexes() {
                var xData = this.getLineData(this.xAxisName);
                var start = 1;

                while (xData[start] < this.displayChartXRange.start) {
                    start++;
                }

                if (start - 1 >= 1) {
                    start--;
                }

                var end = xData.length - 1;

                while (xData[end] > this.displayChartXRange.end) {
                    end--;
                }

                if (end + 1 <= xData.length - 1) {
                    end++;
                }

                this.displayChartXIndexesRangePrev = this.displayChartXIndexesRange.clone();
                this.displayChartXIndexesRange = new ChartRange(start, end);
            }
        }, {
            key: "calculateMaxChartYAxis",
            value: function calculateMaxChartYAxis() {
                this.displayChartYRangePrev = this.displayChartYRange.clone();
                this.displayChartYRange = new ChartRange(this.getYAxisMinValue(), this.getYAxisMaxValue());

                if (this.options.extendAxisYToZero && !this.displayChartYRange.isContain(0)) {
                    if (this.displayChartYRange.start > 0) {
                        this.displayChartYRange.start = 0;
                    } else {
                        this.displayChartYRange.end = 0;
                    }
                }
            }
        }, {
            key: "draw",
            value: function draw() {
                this.rootElement.removeChilds();
                this.drawRootSvgElement();
                this.rootElement.appendChild(this.sections.rootSvg);
            }
        }, {
            key: "drawRootSvgElement",
            value: function drawRootSvgElement() {
                this.sections.rootSvg = document.createSvgElement("svg");
                this.sections.rootSvg.setAttribute("width", this.options.width);
                this.sections.rootSvg.setAttribute("height", this.options.height);
                this.sections.rootSvg.setAttribute("viewBox", "0 0 " + this.options.width + " " + this.options.height);

                if (this.options.showAxisY) {
                    this.drawYAxis();
                    this.sections.rootSvg.appendChild(this.sections.yAxis);
                }

                if (this.options.showAxisX) {
                    this.drawXAxis();
                    this.sections.rootSvg.appendChild(this.sections.xAxis);
                }

                this.drawSvgLines();
                this.sections.rootSvg.appendChild(this.sections.linesGroup);

                if (this.options.showDataOnHover) {
                    this.showDataOnHover();
                    this.sections.rootSvg.appendChild(this.sections.showDataOnHoverElement);
                }

                return this.sections.rootSvg;
            }
        }, {
            key: "showDataOnHover",
            value: function showDataOnHover() {
                var self = this;
                self.sections.showDataOnHoverElement = document.createSvgElement("g");
                self.sections.showDataOnHoverElement.setAttribute("id", "show-data-tooltip");
                this.sections.rootSvg.addEventListener("mousemove", function (event) {
                    var relativeXCoord = self.displayChartXRange.start + (event.offsetX - self.absoluteXRange.start) / self.absoluteXRange.getDifference() * self.displayChartXRange.getDifference();
                    var xData = self.getLineData(self.xAxisName);
                    var hoverIndex = -1;

                    for (var i = self.displayChartXIndexesRange.start; i < self.displayChartXIndexesRange.end; i++) {
                        if (relativeXCoord >= xData[i] && relativeXCoord <= xData[i + 1]) {
                            if (Math.abs(relativeXCoord - xData[i]) < Math.abs(xData[i + 1] - relativeXCoord)) {
                                hoverIndex = i;
                            } else {
                                hoverIndex = i + 1;
                            }

                            break;
                        }
                    }

                    if (hoverIndex > self.displayChartXIndexesRange.start && hoverIndex < self.displayChartXIndexesRange.end) {
                        self.showDataTooltip(hoverIndex, xData[hoverIndex]);
                    }
                });
                this.sections.rootSvg.addEventListener("mouseout", function (event) {
                    self.deleteDataTooltip();
                });
            }
        }, {
            key: "showDataTooltip",
            value: function showDataTooltip(xIndex, xValue) {
                if (this.showingDataTooltipIndex != xIndex) {
                    this.showingDataTooltipIndex = xIndex;
                    this.deleteDataTooltip();
                    var xCoord = this.calculateXCoord(xValue, this.displayChartXRange);
                    var tooltipXCoord = xCoord + this.tooltipCircleRadius;

                    if (tooltipXCoord + this.showingDataTooltipWidth > this.absoluteXRange.end) {
                        tooltipXCoord = xCoord - this.showingDataTooltipWidth - this.tooltipCircleRadius;
                    }

                    var forObj = document.createSvgElement("foreignObject");
                    this.sections.showDataOnHoverElement.appendChild(forObj);
                    forObj.setAttribute("class", "node");
                    forObj.setAttribute("x", tooltipXCoord);
                    forObj.setAttribute("y", this.tooltipCircleRadius);
                    forObj.setAttribute("width", this.showingDataTooltipWidth);
                    forObj.setAttribute("height", this.absoluteYRange.getDifference());
                    var divEl = document.createElement("div");
                    forObj.appendChild(divEl);
                    divEl.setAttribute("class", "data-tooltip");
                    var dateDiv = document.createElement("div");
                    divEl.appendChild(dateDiv);
                    dateDiv.setAttribute("class", "date");
                    dateDiv.innerHTML = new Date(xValue).toShortStringWithDayOfWeek();
                    var linesDiv = document.createElement("div");
                    divEl.appendChild(linesDiv);
                    linesDiv.setAttribute("class", "chart-data");

                    for (var i = 0; i < this.lines.length; i++) {
                        var lineData = this.getLineData(this.lines[i].name);
                        var lineDiv = document.createElement("div");
                        linesDiv.appendChild(lineDiv);
                        lineDiv.setAttribute("class", "line-data");
                        lineDiv.style.cssText = "color: " + this.lines[i].color + ";";
                        lineDiv.innerHTML = "<div class='number'>" + lineData[xIndex] + "</div><div class='title'>" + this.lines[i].title + "</div>";
                        var verticalLine = document.createSvgElement("path");
                        this.sections.showDataOnHoverElement.appendChild(verticalLine);
                        verticalLine.setAttribute("opacity", "0.7");
                        verticalLine.setAttribute("fill", "none");
                        verticalLine.setAttribute("stroke", "#808080");
                        verticalLine.setAttribute("d", "M " + xCoord + " 20 V " + this.absoluteYRange.end.toString());
                        var circleEl = document.createSvgElement("circle");
                        circleEl.setAttribute("class", "tooltip-label");
                        circleEl.setAttribute("opacity", "0.7");
                        circleEl.setAttribute("cx", xCoord);
                        circleEl.setAttribute("cy", this.calculateYCoord(lineData[xIndex], this.displayChartYRange));
                        circleEl.setAttribute("r", this.tooltipCircleRadius);
                        circleEl.setAttribute("stroke-width", 3);
                        circleEl.setAttribute("stroke", this.lines[i].color);
                        circleEl.setAttribute("fill", "none");
                        this.sections.showDataOnHoverElement.appendChild(circleEl);
                    }
                }
            }
        }, {
            key: "deleteDataTooltip",
            value: function deleteDataTooltip() {
                this.sections.showDataOnHoverElement.removeChilds();
            }
        }, {
            key: "drawYAxis",
            value: function drawYAxis() {
                if (this.sections.yAxis == null) {
                    this.sections.yAxis = document.createSvgElement("g");
                } else {
                    this.sections.yAxis.removeChilds();
                }

                this.sections.yAxis.setAttribute("id", "yAxis");
                this.sections.yAxis.setAttribute("opacity", "0.7");
                var rangeYParts = this.divideRangeEqualParts(this.displayChartYRange, this.options.yAxisLinesCount - 1);
                this.drawYAxisNewLines(rangeYParts, this.displayChartXRange, this.displayChartXRange, this.displayChartYRange, this.displayChartYRange);
                return this.sections.yAxis;
            }
        }, {
            key: "drawYAxisNewLines",
            value: function drawYAxisNewLines(rangeYParts, xRange, xRangePrev, yRange, yRangePrev) {
                var promises = [];

                for (var i = 0; i < rangeYParts.length; i++) {
                    var horizontalLineEl = document.createSvgElement("path");
                    this.sections.yAxis.appendChild(horizontalLineEl);
                    horizontalLineEl.setAttribute("fill", "none");
                    horizontalLineEl.setAttribute("stroke", "#808080");
                    var textEl = document.createSvgElement("text");
                    this.sections.yAxis.appendChild(textEl);
                    textEl.innerHTML = rangeYParts[i].toString();
                    textEl.setAttribute("class", "axis-label");
                    textEl.setAttribute("opacity", "0");
                    textEl.setAttribute("transform", "translate(0,-5)");
                    this.sections.yAxisLinesElements[rangeYParts[i]] = {
                        yRealCoord: rangeYParts[i],
                        horizontalLineEl: horizontalLineEl,
                        textEl: textEl
                    };
                    var promise = this.redrawYAxisLine(rangeYParts[i], horizontalLineEl, textEl, xRange, xRangePrev, yRange, yRangePrev, 1, 0);
                    promises.push(promise);
                }

                return Promise.all(promises);
            }
        }, {
            key: "redrawYAxis",
            value: function redrawYAxis() {
                var _this = this;

                var self = this;
                var promises = [];
                var newRangeYParts = this.divideRangeEqualParts(this.displayChartYRange, this.options.yAxisLinesCount - 1);

                var _loop = function _loop(prop) {
                    if (!_this.sections.yAxisLinesElements.hasOwnProperty(prop)) {
                        return "continue";
                    }

                    var prevYRealCoord = _this.sections.yAxisLinesElements[prop].yRealCoord;
                    var lineEl = _this.sections.yAxisLinesElements[prop].horizontalLineEl;
                    var textEl = _this.sections.yAxisLinesElements[prop].textEl;

                    if (newRangeYParts.indexOf(prevYRealCoord) < 0) {
                        var _promise2 = _this.redrawYAxisLine(prevYRealCoord, lineEl, textEl, _this.displayChartXRange, _this.displayChartXRangePrev, _this.displayChartYRange, _this.displayChartYRangePrev, 0, 1);

                        promises.push(_promise2);

                        _promise2.then(function () {
                            lineEl.remove();
                            textEl.remove();
                            delete self.sections.yAxisLinesElements[prevYRealCoord];
                        });
                    } else {
                        var _promise3 = _this.redrawYAxisLine(prevYRealCoord, lineEl, textEl, _this.displayChartXRange, _this.displayChartXRangePrev, _this.displayChartYRange, _this.displayChartYRangePrev, 1, 1);

                        promises.push(_promise3);
                        newRangeYParts.splice(newRangeYParts.indexOf(prevYRealCoord), 1);
                    }
                };

                for (var prop in this.sections.yAxisLinesElements) {
                    var _ret = _loop(prop);

                    if (_ret === "continue") continue;
                }

                var promise = this.drawYAxisNewLines(newRangeYParts, this.displayChartXRange, this.displayChartXRangePrev, this.displayChartYRange, this.displayChartYRangePrev);
                promises.push(promise);
                return Promise.all(promises);
            }
        }, {
            key: "redrawYAxisLine",
            value: function redrawYAxisLine(yRealCoord, lineEl, textEl, xRange, xRangePrev, yRange, yRangePrev, opacity, opacityPrev) {
                var self = this;
                return new Promise(function (resolve, reject) {
                    var framesCount = self.animateFramesCount;
                    var xPrev = xRangePrev.clone();
                    var yPrev = yRangePrev.clone();
                    var xStartDif = (xRange.start - xPrev.start) / framesCount;
                    var xEndDif = (xRange.end - xPrev.end) / framesCount;
                    var yStartDif = (yRange.start - yPrev.start) / framesCount;
                    var yEndDif = (yRange.end - yPrev.end) / framesCount;
                    var opacityDif = (opacity - opacityPrev) / framesCount;
                    var currentFrame = 0;

                    var rafCallback = function rafCallback(timestamp) {
                        xPrev.start += xStartDif;
                        xPrev.end += xEndDif;
                        yPrev.start += yStartDif;
                        yPrev.end += yEndDif;
                        opacityPrev += opacityDif;
                        var calcualteYCoord = self.calculateYCoord(yRealCoord, yPrev);
                        var calcualteXCoord = self.calculateXCoord(xPrev.start, xPrev);
                        var linePathD = "M " + calcualteXCoord + " " + calcualteYCoord + " H " + self.calculateXCoord(xPrev.end, xPrev);
                        lineEl.setAttribute("d", linePathD);
                        lineEl.setAttribute("opacity", opacityPrev);
                        textEl.setAttribute("x", calcualteXCoord);
                        textEl.setAttribute("y", calcualteYCoord);
                        textEl.setAttribute("opacity", opacityPrev);
                        currentFrame++;

                        if (currentFrame < framesCount) {
                            requestAnimationFrame(rafCallback);
                        } else {
                            resolve();
                        }
                    };

                    requestAnimationFrame(rafCallback);
                });
            }
        }, {
            key: "divideRangeEqualParts",
            value: function divideRangeEqualParts(range, partsCount) {
                var partDif = range.getDifference() / partsCount;

                if (partDif == 0) {
                    return [range.start];
                }

                var partDifLog10 = Math.trunc(Math.log10(partDif));
                var pow10 = Math.pow(10, partDifLog10);
                var rounedValue;

                if (partDif / pow10 >= 5) {
                    rounedValue = pow10;
                } else if (partDif / pow10 >= 1.5) {
                    rounedValue = pow10 / 2;
                } else {
                    rounedValue = pow10 / 10;
                }

                partDif = partDif - partDif % rounedValue;
                var result = [];
                var rangeStart = range.start - range.start % rounedValue;

                if (rangeStart < range.start) {
                    rangeStart += rounedValue;
                }

                for (var i = 0; i < partsCount + 1; i++) {
                    var point = rangeStart + i * partDif;
                    result.push(point);
                }

                return result;
            }
        }, {
            key: "drawXAxis",
            value: function drawXAxis() {
                if (this.sections.xAxis == null) {
                    this.sections.xAxis = document.createSvgElement("g");
                } else {
                    this.sections.xAxis.removeChilds();
                }

                this.drawNewXAxis();
            }
        }, {
            key: "drawNewXAxis",
            value: function drawNewXAxis() {
                var promises = [];
                var xData = this.getLineData(this.xAxisName);
                var axisLabelsIndexes = this.getDisplayingXAxisLabelsIndexes();

                for (var i = 0; i < axisLabelsIndexes.length; i++) {
                    var labelIndex = axisLabelsIndexes[i];
                    var labelValue = xData[labelIndex];
                    var labelEl = this.createXAxisLabelElement(labelValue);
                    this.sections.xAxis.appendChild(labelEl);
                    this.sections.xAxisElements[labelIndex] = {
                        index: labelIndex,
                        value: labelValue,
                        labelEl: labelEl
                    };
                    var promise = this.redrawXAxisLabel(labelEl, labelValue, this.displayChartXRange, this.displayChartXRange, 1, 0);
                    promises.push(promise);
                }

                return Promise.all(promises);
            }
        }, {
            key: "createXAxisLabelElement",
            value: function createXAxisLabelElement(labelValue) {
                var labelEl = document.createSvgElement("text");
                labelEl.innerHTML = new Date(labelValue).toShortString();
                labelEl.setAttribute("class", "axis-label");
                labelEl.setAttribute("text-anchor", "middle");
                labelEl.setAttribute("y", this.absoluteYRange.end);
                labelEl.setAttribute("transform", "translate(0, 16)");
                labelEl.setAttribute("opacity", "0");
                return labelEl;
            }
        }, {
            key: "getDisplayingXAxisLabelsIndexes",
            value: function getDisplayingXAxisLabelsIndexes() {
                var xData = this.getLineData(this.xAxisName);
                var xDataIndexes = [];

                for (var i = 1; i < xData.length; i++) {
                    if (xData[i] >= this.displayChartXRange.start && xData[i] <= this.displayChartXRange.end) {
                        xDataIndexes.push(i);
                    }
                }

                if (xDataIndexes.length <= this.options.xAxisLabelsCount) {
                    return xDataIndexes;
                }

                var result = [];
                var interval = xDataIndexes.length / this.options.xAxisLabelsCount;

                for (var _i = 0; _i < this.options.xAxisLabelsCount; _i++) {
                    var ind = Math.round(interval * _i + interval / 2);
                    result.push(xDataIndexes[ind]);
                }

                return result;
            }
        }, {
            key: "redrawXAxis",
            value: function redrawXAxis() {
                var _this2 = this;

                var self = this;
                var promises = [];
                var labelWidth = this.defaultXAxisLabelAbsoluteWidth;
                var halfLabelWidth = this.defaultXAxisLabelAbsoluteWidth / 2;
                var xData = this.getLineData(this.xAxisName);
                var newDisplayingLables = [];

                var _loop2 = function _loop2(prop) {
                    if (!_this2.sections.xAxisElements.hasOwnProperty(prop)) {
                        return "continue";
                    }

                    var xLabel = _this2.sections.xAxisElements[prop];

                    var newCoord = _this2.calculateXCoord(xLabel.value, _this2.displayChartXRange); // delete that is not in range


                    if (newCoord - halfLabelWidth < _this2.absoluteXRange.start || newCoord + halfLabelWidth > _this2.absoluteXRange.end) {
                        var _promise8 = _this2.redrawXAxisLabel(xLabel.labelEl, xLabel.value, _this2.displayChartXRange, _this2.displayChartXRangePrev, 0, 1);

                        promises.push(_promise8);

                        _promise8.then(function () {
                            xLabel.labelEl.remove();
                        });
                    } else {
                        xLabel.newCoord = newCoord;
                        newDisplayingLables.push(xLabel);
                    }
                };

                for (var prop in this.sections.xAxisElements) {
                    var _ret2 = _loop2(prop);

                    if (_ret2 === "continue") continue;
                } // dleete elements between, if they are a lot.


                if (newDisplayingLables.length > this.options.xAxisLabelsCount) {
                    var _loop3 = function _loop3(i) {
                        var xLabel = newDisplayingLables[i];

                        var promise = _this2.redrawXAxisLabel(xLabel.labelEl, xLabel.value, _this2.displayChartXRange, _this2.displayChartXRangePrev, 0, 1);

                        promises.push(promise);
                        promise.then(function () {
                            xLabel.labelEl.remove();
                        });
                        newDisplayingLables.splice(i, 1);
                    };

                    for (var i = 1; i < newDisplayingLables.length; i++) {
                        _loop3(i);
                    }
                } //redraw which remains


                for (var i = 0; i < newDisplayingLables.length; i++) {
                    var promise = this.redrawXAxisLabel(newDisplayingLables[i].labelEl, newDisplayingLables[i].value, this.displayChartXRange, this.displayChartXRangePrev, 1, 1);
                    promises.push(promise);
                } // add element to the left and to the right


                if (newDisplayingLables.length > 0) {
                    while (newDisplayingLables[0].newCoord - labelWidth > this.absoluteXRange.start + halfLabelWidth) {
                        // create new Elements
                        var newXLabelIndex = 0;

                        if (newDisplayingLables.length > 1) {
                            newXLabelIndex = newDisplayingLables[0].index - (newDisplayingLables[1].index - newDisplayingLables[0].index);
                        } else {
                            newXLabelIndex = Math.round(newDisplayingLables[0].index - this.displayChartXIndexesRange.getDifference() / this.options.xAxisLabelsCount);
                        }

                        if (newXLabelIndex >= 1 && newXLabelIndex < xData.length) {
                            var newXLabelValue = xData[newXLabelIndex];
                            var newLabelEl = self.createXAxisLabelElement(newXLabelValue);
                            this.sections.xAxis.appendChild(newLabelEl);
                            var newCoord = this.calculateXCoord(newXLabelValue, this.displayChartXRange);
                            var newLabel = {
                                index: newXLabelIndex,
                                value: newXLabelValue,
                                labelEl: newLabelEl,
                                newCoord: newCoord
                            };
                            newDisplayingLables.splice(0, 0, newLabel);

                            var _promise4 = this.redrawXAxisLabel(newLabelEl, newXLabelValue, this.displayChartXRange, this.displayChartXRangePrev, 1, 0);

                            promises.push(_promise4);
                        } else {
                            break;
                        }
                    }

                    while (newDisplayingLables[newDisplayingLables.length - 1].newCoord + labelWidth < this.absoluteXRange.end - halfLabelWidth) {
                        // create new Elements
                        var _newXLabelIndex = 0;

                        if (newDisplayingLables.length > 1) {
                            _newXLabelIndex = newDisplayingLables[newDisplayingLables.length - 1].index + (newDisplayingLables[1].index - newDisplayingLables[0].index);
                        } else {
                            _newXLabelIndex = Math.round(newDisplayingLables[newDisplayingLables.length - 1].index + this.displayChartXIndexesRange.getDifference() / this.options.xAxisLabelsCount);
                        }

                        if (_newXLabelIndex >= 0 && _newXLabelIndex < xData.length) {
                            var _newXLabelValue = xData[_newXLabelIndex];

                            var _newLabelEl = self.createXAxisLabelElement(_newXLabelValue);

                            this.sections.xAxis.appendChild(_newLabelEl);

                            var _newCoord = this.calculateXCoord(_newXLabelValue, this.displayChartXRange);

                            var _newLabel = {
                                index: _newXLabelIndex,
                                value: _newXLabelValue,
                                labelEl: _newLabelEl,
                                newCoord: _newCoord
                            };
                            newDisplayingLables.push(_newLabel);

                            var _promise5 = this.redrawXAxisLabel(_newLabelEl, _newXLabelValue, this.displayChartXRange, this.displayChartXRangePrev, 1, 0);

                            promises.push(_promise5);
                        } else {
                            break;
                        }
                    }
                } // create new elements between, if they are few


                if (newDisplayingLables.length <= Math.trunc(this.options.xAxisLabelsCount / 2)) {
                    for (var _i2 = 1; _i2 < newDisplayingLables.length; _i2 = _i2 + 2) {
                        var _newXLabelIndex2 = Math.round((newDisplayingLables[_i2].index + newDisplayingLables[_i2 - 1].index) / 2);

                        var _newXLabelValue2 = xData[_newXLabelIndex2];

                        var _newLabelEl2 = self.createXAxisLabelElement(_newXLabelValue2);

                        this.sections.xAxis.appendChild(_newLabelEl2);

                        var _newCoord2 = this.calculateXCoord(_newXLabelValue2, this.displayChartXRange);

                        var _newLabel2 = {
                            index: _newXLabelIndex2,
                            value: _newXLabelValue2,
                            labelEl: _newLabelEl2,
                            newCoord: _newCoord2
                        };
                        newDisplayingLables.splice(_i2, 0, _newLabel2);

                        var _promise6 = this.redrawXAxisLabel(_newLabelEl2, _newXLabelValue2, this.displayChartXRange, this.displayChartXRangePrev, 1, 0);

                        promises.push(_promise6);
                    }
                }

                delete this.sections.xAxisElements;
                this.sections.xAxisElements = {};

                for (var _i3 = 0; _i3 < newDisplayingLables.length; _i3++) {
                    this.sections.xAxisElements[newDisplayingLables[_i3].index] = newDisplayingLables[_i3];
                }

                if (newDisplayingLables.length == 0) {
                    var _promise7 = this.drawNewXAxis();

                    promises.push(_promise7);
                }

                return Promise.all(promises);
            }
        }, {
            key: "redrawXAxisLabel",
            value: function redrawXAxisLabel(labelEl, xLabelValue, xRange, xRangePrev, opacity, opacityPrev) {
                var self = this;
                return new Promise(function (resolve, reject) {
                    var framesCount = self.animateFramesCount;
                    var xPrev = xRangePrev.clone();
                    var xStartDif = (xRange.start - xPrev.start) / framesCount;
                    var xEndDif = (xRange.end - xPrev.end) / framesCount;
                    var opacityDif = (opacity - opacityPrev) / framesCount;
                    var currentFrame = 0;

                    var rafCallback = function rafCallback(timestamp) {
                        xPrev.start += xStartDif;
                        xPrev.end += xEndDif;
                        opacityPrev += opacityDif;
                        var x = self.calculateXCoord(xLabelValue, xPrev);
                        labelEl.setAttribute("x", x);
                        labelEl.setAttribute("opacity", opacityPrev);
                        currentFrame++;

                        if (currentFrame < framesCount) {
                            requestAnimationFrame(rafCallback);
                        } else {
                            resolve();
                        }
                    };

                    requestAnimationFrame(rafCallback);
                });
            }
        }, {
            key: "redrawSvgLines",
            value: function redrawSvgLines() {
                var promises = [];

                for (var i = 0; i < this.lines.length; i++) {
                    var line = this.lines[i];
                    var lineEl = this.sections.linesElements[line.name];

                    if (line.visible == true) {
                        lineEl.setAttribute("visibility", "visible");
                        var promise = this.redrawLineAnimate(line, lineEl, this.displayChartXRange, this.displayChartXRangePrev, this.displayChartYRange, this.displayChartYRangePrev, this.displayChartXIndexesRange, this.displayChartXIndexesRangePrev);
                        promises.push(promise);
                    } else {
                        lineEl.setAttribute("visibility", "hidden");
                    }
                }

                return Promise.all(promises);
            }
        }, {
            key: "redrawLineAnimate",
            value: function redrawLineAnimate(line, lineEl, xRange, xRangePrev, yRange, yRangePrev, xIndexesRange, xIndexesRangePrev) {
                var self = this;
                return new Promise(function (resolve, reject) {
                    var framesCount = self.animateFramesCount;
                    var xPrev = xRangePrev.clone();
                    var yPrev = yRangePrev.clone();
                    var xIndexPrev = xIndexesRangePrev.clone();
                    var xStartDif = (xRange.start - xPrev.start) / framesCount;
                    var xEndDif = (xRange.end - xPrev.end) / framesCount;
                    var yStartDif = (yRange.start - yPrev.start) / framesCount;
                    var yEndDif = (yRange.end - yPrev.end) / framesCount;
                    var xIStartDif = (xIndexesRange.start - xIndexesRangePrev.start) / framesCount;
                    var xIEndDif = (xIndexesRange.end - xIndexesRangePrev.end) / framesCount;
                    var currentFrame = 0;

                    var rafCallback = function rafCallback(timestamp) {
                        xPrev.start += xStartDif;
                        xPrev.end += xEndDif;
                        yPrev.start += yStartDif;
                        yPrev.end += yEndDif;
                        xIndexPrev.start += xIStartDif;
                        xIndexPrev.end += xIEndDif;
                        lineEl.setAttribute("d", self.getSvgLinePathD(line.name, xPrev, yPrev, new ChartRange(Math.round(xIndexPrev.start), Math.round(xIndexPrev.end))));
                        currentFrame++;

                        if (currentFrame < framesCount) {
                            requestAnimationFrame(rafCallback);
                        } else {
                            resolve();
                        }
                    };

                    requestAnimationFrame(rafCallback);
                });
            }
        }, {
            key: "drawSvgLines",
            value: function drawSvgLines() {
                var self = this;
                this.sections.linesGroup = document.createSvgElement("g");
                this.sections.linesGroup.setAttribute("id", this.rootElementId + "lines-group");
                this.sections.linesGroup.setAttribute("stroke-width", this.getLineStrokeWidth());
                var linesEl = this.drawSvgLineElements();
                linesEl.forEach(function (lineEl) {
                    self.sections.linesGroup.appendChild(lineEl);
                });
                return this.sections.linesGroup;
            }
        }, {
            key: "drawSvgLineElements",
            value: function drawSvgLineElements() {
                var result = [];

                for (var i = 0; i < this.lines.length; i++) {
                    if (this.lines[i].visible) {
                        var svgLine = this.createSvgLineElement(this.lines[i]);
                        this.sections.linesElements[this.lines[i].name] = svgLine;
                        result.push(svgLine);
                    }
                }

                return result;
            }
        }, {
            key: "createSvgLineElement",
            value: function createSvgLineElement(line) {
                var pathEl = document.createSvgElement("path");
                pathEl.setAttribute("fill", "none");
                pathEl.setAttribute("opacity", "0.7");
                pathEl.setAttribute("stroke-linejoin", "round");
                pathEl.setAttribute("stroke", line.color);
                pathEl.setAttribute("d", this.getSvgLinePathD(line.name, this.displayChartXRange, this.displayChartYRange, this.displayChartXIndexesRange));
                return pathEl;
            }
        }, {
            key: "getSvgLinePathD",
            value: function getSvgLinePathD(lineName, displayChartXRange, displayChartYRange, displayChartXIndexesRange) {
                var result = "";
                var xData = this.getLineData(this.xAxisName);
                var lineData = this.getLineData(lineName);
                var i = displayChartXIndexesRange.start; // draw first point

                if (i <= displayChartXIndexesRange.end) {
                    result += "M " + this.calculateXCoord(xData[i], displayChartXRange) + " " + this.calculateYCoord(lineData[i], displayChartYRange) + " ";
                    i++;
                } // add points that is before end of display range


                while (i <= displayChartXIndexesRange.end) {
                    result += "L " + this.calculateXCoord(xData[i], displayChartXRange) + " " + this.calculateYCoord(lineData[i], displayChartYRange) + " ";
                    i++;
                }

                return result;
            }
        }, {
            key: "calculateXCoord",
            value: function calculateXCoord(realXValue, displayRange) {
                var displayRangeDif = displayRange.getDifference();

                if (displayRangeDif == 0) {
                    return this.absoluteXRange.start;
                }

                return (realXValue - displayRange.start) / displayRangeDif * this.absoluteXRange.getDifference() + this.absoluteXRange.start;
            }
        }, {
            key: "calculateYCoord",
            value: function calculateYCoord(realYValue, displayRange) {
                var displayRangeDif = displayRange.getDifference();

                if (displayRangeDif == 0) {
                    return this.absoluteYRange.start;
                }

                var yCoord = (realYValue - displayRange.start) / displayRangeDif * this.absoluteYRange.getDifference() + this.absoluteYRange.start;
                return this.invertYAxisValue(yCoord);
            }
        }, {
            key: "invertYAxisValue",
            value: function invertYAxisValue(value) {
                return this.options.height - value;
            }
        }, {
            key: "getBgColor",
            value: function getBgColor() {
                //return "#242f3e";
                return this.mode == "night" ? "#242f3e" : "#ffffff";
            }
        }, {
            key: "getXAxisName",
            value: function getXAxisName() {
                var self = this;

                if (!this.initialChartData.types) {
                    throw "initialChartData.types is undefined";
                }

                var propName = null;
                this.initialChartData.types.getOwnProperties().forEach(function (prop, i) {
                    if (self.initialChartData.types[prop] === "x") {
                        propName = prop;
                    }
                });

                if (propName == null) {
                    throw "initialChartData.types do not contains";
                }

                return propName;
            }
        }, {
            key: "getLines",
            value: function getLines() {
                var self = this;

                if (!this.initialChartData.types) {
                    throw "initialChartData.types is undefined";
                }

                var propNames = [];
                this.initialChartData.types.getOwnProperties().forEach(function (prop, i) {
                    if (self.initialChartData.types[prop] === "line") {
                        propNames.push(new ChartLine(prop, true, self.getLineTitle(prop), self.getLineColor(prop)));
                    }
                });
                return propNames;
            }
        }, {
            key: "getXAxisMinValue",
            value: function getXAxisMinValue() {
                var xData = this.getLineData(this.xAxisName);
                return xData[1];
            }
        }, {
            key: "getXAxisMaxValue",
            value: function getXAxisMaxValue() {
                var xData = this.getLineData(this.xAxisName);
                return xData[xData.length - 1];
            }
        }, {
            key: "getYAxisMinValue",
            value: function getYAxisMinValue() {
                var firstVisibleLine = this.getFirstVisibleLine();

                if (firstVisibleLine == null) {
                    return 0;
                }

                var min = this.getLineMinValue(firstVisibleLine);

                for (var i = 1; i < this.lines.length; i++) {
                    if (this.lines[i].visible === true) {
                        var tempMin = this.getLineMinValue(this.lines[i]);

                        if (tempMin < min) {
                            min = tempMin;
                        }
                    }
                }

                return min;
            }
        }, {
            key: "getLineMinValue",
            value: function getLineMinValue(line) {
                var lineData = this.getLineData(line.name);

                if (lineData.length <= 1) {
                    return 0;
                }

                var min = lineData[this.displayChartXIndexesRange.start];

                for (var i = this.displayChartXIndexesRange.start + 1; i <= this.displayChartXIndexesRange.end; i++) {
                    if (lineData[i] < min) {
                        min = lineData[i];
                    }
                }

                return min;
            }
        }, {
            key: "getYAxisMaxValue",
            value: function getYAxisMaxValue() {
                var firstVisibleLine = this.getFirstVisibleLine();

                if (firstVisibleLine == null) {
                    return 0;
                }

                var max = this.getLineMaxValue(firstVisibleLine);

                for (var i = 1; i < this.lines.length; i++) {
                    if (this.lines[i].visible) {
                        var tempMax = this.getLineMaxValue(this.lines[i]);

                        if (tempMax > max) {
                            max = tempMax;
                        }
                    }
                }

                return max;
            }
        }, {
            key: "getLineMaxValue",
            value: function getLineMaxValue(line) {
                var lineData = this.getLineData(line.name);

                if (lineData.length <= 1) {
                    return 0;
                }

                var max = lineData[this.displayChartXIndexesRange.start];

                for (var i = this.displayChartXIndexesRange.start + 1; i <= this.displayChartXIndexesRange.end; i++) {
                    if (lineData[i] > max) {
                        max = lineData[i];
                    }
                }

                return max;
            }
        }, {
            key: "getFirstVisibleLine",
            value: function getFirstVisibleLine() {
                for (var i = 0; i < this.lines.length; i++) {
                    if (this.lines[i].visible === true) {
                        return this.lines[i];
                    }
                }

                return null;
            }
        }, {
            key: "getLineData",
            value: function getLineData(lineName) {
                if (!this.initialChartData.columns) {
                    throw "initialChartData.columns is undefined";
                }

                for (var i = 0; i < this.initialChartData.columns.length; i++) {
                    if (this.initialChartData.columns[i][0] === lineName) return this.initialChartData.columns[i];
                }

                throw "initialChartData.columns doesn't contain " + lineName;
            }
        }, {
            key: "getLineColor",
            value: function getLineColor(lineName) {
                if (!this.initialChartData.colors) {
                    throw "initialChartData.colors is undefined";
                }

                var color = this.initialChartData.colors[lineName];

                if (!color) {
                    throw "initialChartData.colors doesn't contain " + lineName;
                }

                return color;
            }
        }, {
            key: "getLineTitle",
            value: function getLineTitle(lineName) {
                if (!this.initialChartData.names) {
                    throw "initialChartData.names is undefined";
                }

                var name = this.initialChartData.names[lineName];
                return name;
            }
        }, {
            key: "getLineStrokeWidth",
            value: function getLineStrokeWidth() {
                var smallerSide = this.options.height < this.options.width ? this.options.height : this.options.width;

                if (smallerSide <= 10) {
                    return 1;
                }

                if (smallerSide <= 100) {
                    return 2;
                }

                if (smallerSide <= 1000) {
                    return 4;
                }

                return 8;
            }
        }]);

        return BaseChart;
    }();

var BaseChartOptions = function BaseChartOptions() {
    _classCallCheck(this, BaseChartOptions);

    _defineProperty(this, "mode", void 0);

    _defineProperty(this, "showAxisX", void 0);

    _defineProperty(this, "showAxisY", void 0);

    _defineProperty(this, "extendAxisYToZero", void 0);

    _defineProperty(this, "yAxisLinesCount", void 0);

    _defineProperty(this, "width", void 0);

    _defineProperty(this, "height", void 0);

    _defineProperty(this, "showDataOnHover", void 0);

    this.mode = "day";
    this.showAxisX = true;
    this.showAxisY = true;
    this.extendAxisYToZero = false;
    this.yAxisLinesCount = 6;
    this.xAxisLabelsCount = 6;
    this.width = 500;
    this.height = 500;
    this.showDataOnHover = true;
};

var ChartLine = function ChartLine(name, visible, title, color) {
    _classCallCheck(this, ChartLine);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "visible", void 0);

    _defineProperty(this, "title", void 0);

    _defineProperty(this, "color", void 0);

    this.name = typeof name == "string" ? name : "empty";
    this.visible = typeof visible == "boolean" ? visible : true;
    this.title = typeof title == "string" ? title : name;
    this.color = typeof color == "string" ? color : "#F00";
};

var BaseChartDOMElements = function BaseChartDOMElements() {
    _classCallCheck(this, BaseChartDOMElements);

    _defineProperty(this, "rootSvg", document.createElement("div"));

    _defineProperty(this, "xAxis", document.createElement("div"));

    _defineProperty(this, "yAxis", document.createElement("div"));

    _defineProperty(this, "linesGroup", document.createElement("div"));

    _defineProperty(this, "showDataOnHoverElement", document.createElement("div"));

    _defineProperty(this, "linesElements", {});

    _defineProperty(this, "yAxisLinesElements", {});

    _defineProperty(this, "xAxisElements", {});

    this.rootSvg = null;
    this.xAxis = null;
    this.yAxis = null;
    this.linesGroup = null;
    this.linesElements = {};
    this.yAxisLinesElements = {};
    this.showDataOnHoverElement = null;
};

var ChartRange =
    /*#__PURE__*/
    function () {
        function ChartRange(start, end) {
            _classCallCheck(this, ChartRange);

            _defineProperty(this, "start", 0);

            _defineProperty(this, "end", 0);

            if (typeof start != "number") {
                throw "start must be the number.";
            }

            if (typeof end != "number") {
                throw "start must be the number.";
            }

            if (start > end) {
                throw "start must be less than end.";
            }

            this.start = start;
            this.end = end;
        }

        _createClass(ChartRange, [{
            key: "getDifference",
            value: function getDifference() {
                return this.end - this.start;
            }
        }, {
            key: "extend",
            value: function extend(part) {
                var dif = this.getDifference();
                this.start -= dif * part / 2;
                this.end += dif * part / 2;
            }
        }, {
            key: "isContain",
            value: function isContain(number) {
                return number >= this.start && number <= this.end;
            }
        }, {
            key: "clone",
            value: function clone() {
                return new ChartRange(this.start, this.end);
            }
        }]);

        return ChartRange;
    }();

var ChartGroup =
    /*#__PURE__*/
    function () {
        function ChartGroup(rootElementId, initialChartData, options) {
            _classCallCheck(this, ChartGroup);

            _defineProperty(this, "zoomChartHeight", 70);

            _defineProperty(this, "listLinesSectorHeight", 50);

            _defineProperty(this, "switchModeSectorHeight", 30);

            _defineProperty(this, "rootElementId", "");

            _defineProperty(this, "rootElement", void 0);

            _defineProperty(this, "options", {});

            _defineProperty(this, "initialChartData", void 0);

            _defineProperty(this, "mainChartOptions", void 0);

            _defineProperty(this, "mainChart", void 0);

            _defineProperty(this, "zoomChart", void 0);

            _defineProperty(this, "rangeSelector", void 0);

            _defineProperty(this, "listLinesSector", void 0);

            _defineProperty(this, "switchModeSector", void 0);

            _defineProperty(this, "switchModeTextElement", void 0);

            this.rootElementId = rootElementId;
            this.options = options != null ? options : {};
            this.initialChartData = initialChartData;
            this.rootElement = document.getElementById(this.rootElementId);
        }

        _createClass(ChartGroup, [{
            key: "draw",
            value: function draw() {
                var self = this;
                var height = this.options.height != null ? this.options.height : this.rootElement.clientHeight;

                if (height < 250) {
                    throw "Chart height is too small.";
                }

                var width = this.options.width != null ? this.options.width : this.rootElement.clientWidth;

                if (width < 250) {
                    throw "Chart width is too small.";
                }

                var mainChartId = this.rootElementId + "-main-chart";
                var mainChartHeight = height - this.zoomChartHeight - this.switchModeSectorHeight - this.listLinesSectorHeight;
                var mainChartElement = document.createElement("div");
                mainChartElement.setAttribute("id", mainChartId);
                mainChartElement.style.height = mainChartHeight + "px";
                mainChartElement.style.width = width + "px";
                this.rootElement.appendChild(mainChartElement);
                var zoomChartId = this.rootElementId + "-zoom-chart";
                var rangeChartElement = document.createElement("div");
                rangeChartElement.setAttribute("id", zoomChartId);
                rangeChartElement.style.height = this.zoomChartHeight + "px";
                rangeChartElement.style.width = width + "px";
                this.rootElement.appendChild(rangeChartElement);
                this.zoomChart = new BaseChart(zoomChartId, this.initialChartData, {
                    showAxisY: false,
                    showAxisX: false,
                    extendAxisYToZero: false,
                    showDataOnHover: false,
                    height: this.zoomChartHeight,
                    width: width
                });
                this.zoomChart.draw();
                var mainChartOptions = this.options != null ? this.options : {
                    showAxisY: true,
                    showAxisX: true,
                    extendAxisYToZero: false,
                    showDataOnHover: true
                };
                mainChartOptions.height = mainChartHeight;
                mainChartOptions.width = width;
                this.mainChart = new BaseChart(mainChartId, this.initialChartData, mainChartOptions);
                this.mainChart.draw();
                var initialRangeStart = (this.mainChart.displayChartXRange.start - this.mainChart.chartMaxXRange.start) / this.mainChart.chartMaxXRange.getDifference();
                var initialRangeEnd = (this.mainChart.displayChartXRange.end - this.mainChart.chartMaxXRange.start) / this.mainChart.chartMaxXRange.getDifference();
                var initialRange = new ChartRange(initialRangeStart, initialRangeEnd);
                this.rangeSelector = new RangeSelector(rangeChartElement, initialRange, width);
                this.rangeSelector.onSelectionChange(function (arg) {
                    self.mainChart.setDisplayingXAxisRange(arg.relativeRange);
                });
                this.rangeSelector.draw();
                this.addListLinesSector();
                this.addSwitchModeSector();
            }
        }, {
            key: "addSwitchModeSector",
            value: function addSwitchModeSector() {
                var self = this;
                this.switchModeSector = document.createElement("div");
                this.switchModeSector.style.cssText = "width: 100%; height: " + this.switchModeSectorHeight + "px;" + "display: flex;align-items: flex-end;justify-content: center;";
                this.rootElement.appendChild(this.switchModeSector);
                this.switchModeTextElement = document.createElement("span");
                this.switchModeTextElement.style.cssText = "cursor: pointer; text-align: center; color: #108be3";
                this.switchModeTextElement.innerHTML = "Switch to Night Mode";
                this.switchModeTextElement.addEventListener("click", function (event) {
                    self.switchMode();
                });
                this.switchModeSector.appendChild(this.switchModeTextElement);
            }
        }, {
            key: "addListLinesSector",
            value: function addListLinesSector() {
                var _this3 = this;

                var self = this;
                this.listLinesSector = document.createElement("div");
                this.listLinesSector.setAttribute("class", "lines-list-sector");
                this.listLinesSector.style.cssText = "height: " + this.listLinesSectorHeight + "px;";
                this.rootElement.appendChild(this.listLinesSector);

                var _loop4 = function _loop4(i) {
                    var line = _this3.mainChart.lines[i];
                    var lineElement = document.createElement("span");
                    lineElement.setAttribute("class", "line-select-item line-title");
                    var svgCheckbox = document.createSvgElement("svg");
                    lineElement.appendChild(svgCheckbox);
                    svgCheckbox.setAttribute("width", "25");
                    svgCheckbox.setAttribute("height", "20");
                    var gEl = document.createSvgElement("g");
                    svgCheckbox.appendChild(gEl);
                    gEl.setAttribute("stroke", line.color);
                    gEl.setAttribute("stroke-width", "2");
                    var circleEl = document.createSvgElement("circle");
                    gEl.appendChild(circleEl);
                    circleEl.setAttribute("cx", "10");
                    circleEl.setAttribute("cy", "10");
                    circleEl.setAttribute("r", "8.5");
                    circleEl.setAttribute("fill-opacity", "0");
                    var pathEl = document.createSvgElement("path");
                    gEl.appendChild(pathEl);
                    pathEl.setAttribute("class", "tick-mark");
                    pathEl.setAttribute("d", "M5.2,10 8.5,13.4 14.8,7.2");
                    pathEl.setAttribute("fill", "none");
                    var textEl = document.createElement("span");
                    lineElement.appendChild(textEl);
                    textEl.innerHTML = line.title;
                    lineElement.addEventListener("click", function (event) {
                        if (lineElement.classList.contains("disabled")) {
                            lineElement.classList.remove("disabled");
                            self.mainChart.setLineVisisble(line.name, true);
                            self.zoomChart.setLineVisisble(line.name, true);
                        } else {
                            lineElement.classList.add("disabled");
                            self.mainChart.setLineVisisble(line.name, false);
                            self.zoomChart.setLineVisisble(line.name, false);
                        }
                    });

                    _this3.listLinesSector.appendChild(lineElement);
                };

                for (var i = 0; i < this.mainChart.lines.length; i++) {
                    _loop4(i);
                }
            }
        }, {
            key: "switchMode",
            value: function switchMode() {
                if (this.rootElement.classList.contains("night")) {
                    this.rootElement.classList.remove("night");
                    this.switchModeTextElement.innerHTML = "Switch to Night Mode";
                } else {
                    this.rootElement.classList.add("night");
                    this.switchModeTextElement.innerHTML = "Switch to Day Mode";
                }
            }
        }]);

        return ChartGroup;
    }();

var RangeSelector =
    /*#__PURE__*/
    function () {
        function RangeSelector(rootElement, initialRange, width) {
            _classCallCheck(this, RangeSelector);

            _defineProperty(this, "borderWidth", 7);

            _defineProperty(this, "rootElement", void 0);

            _defineProperty(this, "initialRange", void 0);

            _defineProperty(this, "width", void 0);

            _defineProperty(this, "leftLayer", void 0);

            _defineProperty(this, "centerLayer", void 0);

            _defineProperty(this, "rightLayer", void 0);

            _defineProperty(this, "leftBorder", document.createElement("div"));

            _defineProperty(this, "leftBorderPosX", 0);

            _defineProperty(this, "rightBorder", document.createElement("div"));

            _defineProperty(this, "rightBorderPosX", 0);

            _defineProperty(this, "dragElementOffsetX", void 0);

            _defineProperty(this, "dragElementOffsetY", void 0);

            _defineProperty(this, "onSelectionChangeCallback", null);

            this.rootElement = rootElement;
            this.initialRange = initialRange;
            this.width = width;
        }

        _createClass(RangeSelector, [{
            key: "onSelectionChange",
            value: function onSelectionChange(callback) {
                if (typeof callback != "function") {
                    throw "callback must be a function.";
                }

                this.onSelectionChangeCallback = callback;
            }
        }, {
            key: "draw",
            value: function draw() {
                var self = this;
                this.width = this.width != null ? this.width : this.rootElement.width;
                this.rootElement.style.position = "relative";
                this.rootElement.childNodes.forEach(function (child) {
                    child.style.position = "absolute";
                });
                this.leftBorderPosX = this.initialRange.start * this.width;
                this.rightBorderPosX = this.initialRange.end * this.width;
                var parent = document.createElement("div");
                parent.className = "range-selector";
                this.rootElement.appendChild(parent);
                this.leftLayer = document.createElement("div");
                this.leftLayer.className = "layer dark";
                parent.appendChild(this.leftLayer);
                this.leftBorder = document.createElement("div");
                this.leftBorder.className = "layer border left";
                this.leftBorder.setAttribute("draggable", "true");
                parent.appendChild(this.leftBorder);
                this.centerLayer = document.createElement("div");
                this.centerLayer.className = "layer";
                this.centerLayer.setAttribute("draggable", "true");
                parent.appendChild(this.centerLayer);
                var centerTopLine = document.createElement("div");
                centerTopLine.className = "layer border line top";
                this.centerLayer.appendChild(centerTopLine);
                var centerBottomLine = document.createElement("div");
                centerBottomLine.className = "layer border line bottom";
                this.centerLayer.appendChild(centerBottomLine);
                this.rightBorder = document.createElement("div");
                this.rightBorder.className = "layer border right";
                this.rightBorder.setAttribute("draggable", "true");
                parent.appendChild(this.rightBorder);
                this.rightLayer = document.createElement("div");
                this.rightLayer.className = "layer dark";
                parent.appendChild(this.rightLayer);
                this.updateElementsPositions();

                var onDragStart = function onDragStart(event) {
                    event.dataTransfer.setData('text/plain', 'anything');
                    event.dataTransfer.setDragImage(document.createElement("div"), 0, 0);
                    var style = window.getComputedStyle(event.target, null);
                    self.dragElementOffsetX = parseInt(style.getPropertyValue("left"), 10) - event.clientX;
                    self.dragElementOffsetY = parseInt(style.getPropertyValue("top"), 10) - event.clientY;
                };

                this.rightBorder.addEventListener("dragstart", onDragStart);
                this.leftBorder.addEventListener("dragstart", onDragStart);
                this.centerLayer.addEventListener("dragstart", onDragStart);
                this.rightBorder.addEventListener("drag", function (event) {
                    var newElPosX = event.clientX + self.dragElementOffsetX + self.borderWidth;

                    if (newElPosX >= 0 && newElPosX <= self.width) {
                        if (newElPosX - self.borderWidth > self.leftBorderPosX + self.borderWidth) {
                            //event.target.style.left = newElPosX + "px";
                            self.rightBorderPosX = newElPosX;
                            self.updateElementsPositions();
                            self.selectionChange();
                        }
                    }
                });
                this.leftBorder.addEventListener("drag", function (event) {
                    var newElPosX = event.clientX + self.dragElementOffsetX;

                    if (newElPosX >= 0 && newElPosX <= self.width - self.borderWidth) {
                        if (newElPosX + self.borderWidth < self.rightBorderPosX - self.borderWidth) {
                            //event.target.style.left = newElPosX + "px";
                            self.leftBorderPosX = newElPosX;
                            self.updateElementsPositions();
                            self.selectionChange();
                        }
                    }
                });
                this.centerLayer.addEventListener("drag", function (event) {
                    var leftBorderPosX = event.clientX + self.dragElementOffsetX - self.borderWidth;
                    var rightBorderPosX = self.rightBorderPosX + (leftBorderPosX - self.leftBorderPosX);

                    if (leftBorderPosX >= 0 && rightBorderPosX <= self.width) {
                        self.leftBorderPosX = leftBorderPosX;
                        self.rightBorderPosX = rightBorderPosX;
                        self.updateElementsPositions();
                        self.selectionChange();
                    }
                });
            }
        }, {
            key: "updateElementsPositions",
            value: function updateElementsPositions() {
                this.updateElementLeftAndWidth(this.leftLayer, 0, this.leftBorderPosX);
                this.updateElementLeftAndWidth(this.leftBorder, this.leftBorderPosX, this.borderWidth);
                this.updateElementLeftAndWidth(this.centerLayer, this.leftBorderPosX + this.borderWidth, this.rightBorderPosX - this.leftBorderPosX - this.borderWidth - this.borderWidth);
                this.updateElementLeftAndWidth(this.rightBorder, this.rightBorderPosX - this.borderWidth, this.borderWidth);
                this.updateElementLeftAndWidth(this.rightLayer, this.rightBorderPosX, this.width - this.rightBorderPosX);
            }
        }, {
            key: "updateElementLeftAndWidth",
            value: function updateElementLeftAndWidth(el, left, width) {
                el.style.cssText = "left: " + left + "px;" + "width: " + width + "px;";
            }
        }, {
            key: "selectionChange",
            value: function selectionChange() {
                if (this.onSelectionChangeCallback != null) {
                    var start = this.leftBorderPosX / this.width;
                    var end = this.rightBorderPosX / this.width;
                    this.onSelectionChangeCallback({
                        relativeRange: new ChartRange(start, end)
                    });
                }
            }
        }]);

        return RangeSelector;
    }();