const MonthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
];

const DaysNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

document.addEventListener('DOMContentLoaded', function () {

    let chart1 = new ChartGroup("chart1", chartsData[0], { extendAxisYToZero: true });
    chart1.draw();
    let chart2 = new ChartGroup("chart2", chartsData[1]);
    chart2.draw();
    let chart3 = new ChartGroup("chart3", chartsData[2]);
    chart3.draw();
    let chart4 = new ChartGroup("chart4", chartsData[3]);
    chart4.draw();
    let chart5 = new ChartGroup("chart5", chartsData[4]);
    chart5.draw();

});

Object.prototype.getOwnProperties = function () {
    let result = [];
    for (let name in this) {
        if (this.hasOwnProperty(name)) {
            result.push(name);
        }
    }
    return result;
}

Document.prototype.createSvgElement = function (qualifiedName) {
    return this.createElementNS("http://www.w3.org/2000/svg", qualifiedName);
}

Document.prototype.addCssStyle = function (cssStyle) {
    if (typeof cssStyle != "string") {
        throw "cssStyle must be string.";
    }

    let head = this.head || this.getElementsByTagName('head')[0];
    let style = this.createElement('style');

    head.appendChild(style);

    style.type = 'text/css';
    if (style.styleSheet) {
        // This is required for IE8 and below.
        style.styleSheet.cssText = cssStyle;
    } else {
        style.appendChild(document.createTextNode(cssStyle));
    }
}

HTMLElement.prototype.removeChilds = function () {
    while (this.firstChild) {
        this.removeChild(this.firstChild);
    }
}

SVGElement.prototype.removeChilds = function () {
    while (this.firstChild) {
        this.removeChild(this.firstChild);
    }
}

Date.prototype.toShortString = function () {
    return MonthNames[this.getMonth()] + " " + this.getDate();
}

Date.prototype.toShortStringWithDayOfWeek = function () {
    return DaysNames[this.getDay()] + ", " + MonthNames[this.getMonth()] + " " + this.getDate();
}

class BaseChart {
    defaultChartAxisLabelsHeight = 20;
    animateFramesCount = 7;
    defaultXAxisLabelAbsoluteWidth = 60;
    showingDataTooltipWidth = 150;
    tooltipCircleRadius = 6;

    initialChartData = {};
    options = new BaseChartOptions();

    rootElementId = "";
    rootElement = document.createElement("div");

    absoluteXRange = new ChartRange(0, 0);
    absoluteYRange = new ChartRange(0, 0);
    chartMaxXRange = new ChartRange(0, 0);
    chartMaxYRange = new ChartRange(0, 0);
    displayChartXRange = new ChartRange(0, 0);
    displayChartYRange = new ChartRange(0, 0);
    displayChartXRangePrev = new ChartRange(0, 0);
    displayChartYRangePrev = new ChartRange(0, 0);
    displayChartXIndexesRange = new ChartRange(0, 0);
    displayChartXIndexesRangePrev = new ChartRange(0, 0);

    xAxisName = "";
    lines = [];

    sections = new BaseChartDOMElements();

    redrawPromise = Promise.resolve();
    updateChartRequests = [];
    showingDataTooltipIndex = -1;

    constructor(rootElementId, initialChartData, chartOptions) {
        this.rootElementId = rootElementId;
        this.rootElement = document.getElementById(this.rootElementId);

        this.initialChartData = initialChartData;
        this.mergeOptions(chartOptions);

        this.xAxisName = this.getXAxisName();
        this.lines = this.getLines();

        this.absoluteXRange = new ChartRange(0, this.options.width);
        this.absoluteYRange = new ChartRange(
            this.options.showAxisX ? this.defaultChartAxisLabelsHeight : 0,
            this.options.showAxisY ? this.options.height - this.defaultChartAxisLabelsHeight : this.options.height);

        this.calculateMaxChartXAxis();

        this.redrawPromise = Promise.resolve();
        this.updateChartRequests = [];
    }

    setDisplayingXAxisRange(newRelativeRange) { // newRelativeRange - range start and range end are from 0 to 1;
        this.updateChartRequests.push({ type: "xRange", newRelativeRange: newRelativeRange });
        this.executeRequests();
    }

    setLineVisisble(lineName, visible) {
        this.updateChartRequests.push({ type: "lineVisible", lineName: lineName, visible: visible });
        this.executeRequests();
    }

    executeRequests() {
        let self = this;
        self.redrawPromise = self.redrawPromise.then(function () {
            if (self.updateChartRequests.length == 0) {
                return;
            }

            let newRelativeRange = null;
            let newLinesVisibilities = [];

            for (let i = 0; i < self.updateChartRequests.length; i++) {
                let request = self.updateChartRequests[i];
                if (request.type == "xRange") {
                    newRelativeRange = request.newRelativeRange;
                }
                else if (request.type == "lineVisible") {
                    newLinesVisibilities.push({ lineName: request.lineName, visible: request.visible });
                }
            }

            let lineVisibilityPromise = Promise.resolve();
            if (newLinesVisibilities.length > 0) {
                lineVisibilityPromise = self.executeSetLineVisisbleRequests(newLinesVisibilities);
            }

            let newRangePromise = Promise.resolve();
            if (newRelativeRange != null) {
                newRangePromise = self.executeChangeXAxisRangeRequest(newRelativeRange);
            }

            self.redrawPromise = Promise.all([lineVisibilityPromise, newRangePromise]);
            self.updateChartRequests = [];
        });
    }

    executeChangeXAxisRangeRequest(newRelativeRange) {
        let self = this;
        let newStart = self.chartMaxXRange.start + (newRelativeRange.start * self.chartMaxXRange.getDifference());
        let newEnd = self.chartMaxXRange.start + (newRelativeRange.end * self.chartMaxXRange.getDifference());
        self.displayChartXRangePrev = self.displayChartXRange.clone();
        self.displayChartXRange = new ChartRange(newStart, newEnd);
        self.calculateDisplayChartXDataIndexes();

        return self.redraw();
    }

    executeSetLineVisisbleRequests(linesVisibilities) {
        this.displayChartXRangePrev = this.displayChartXRange.clone();

        let needRedraw = false;
        for (let i = 0; i < linesVisibilities.length; i++) {
            for (let j = 0; j < this.lines.length; j++) {
                if (this.lines[j].name == linesVisibilities[i].lineName) {
                    this.lines[j].visible = linesVisibilities[i].visible;
                    needRedraw = true;
                    break;
                }
            }
        }

        if (needRedraw) {
            return this.redraw();
        }
        else {
            return Promise.resolve();
        }
    }


    mergeOptions(opt) {
        if (opt && opt.mode) {
            this.options.mode = opt.mode;
        }

        if (opt && typeof opt.showAxisX == "boolean") {
            this.options.showAxisX = opt.showAxisX;
        }

        if (opt && typeof opt.showAxisY == "boolean") {
            this.options.showAxisY = opt.showAxisY;
        }

        if (opt && opt.yAxisLinesCount) {
            this.options.yAxisLinesCount = opt.yAxisLinesCount;
        }

        if (opt && opt.xAxisLabelsCount) {
            this.options.xAxisLabelsCount = opt.xAxisLabelsCount;
        }
        let maxXLabelsCount = Math.trunc(opt.width / this.defaultXAxisLabelAbsoluteWidth);
        if (this.options.xAxisLabelsCount > maxXLabelsCount) {
            this.options.xAxisLabelsCount = maxXLabelsCount;
        }

        if (opt && typeof opt.extendAxisYToZero == "boolean") {
            this.options.extendAxisYToZero = opt.extendAxisYToZero;
        }

        if (opt && opt.height) {
            this.options.height = opt.height;
        }

        if (opt && opt.width) {
            this.options.width = opt.width;
        }

        if (opt && typeof opt.showDataOnHover == "boolean") {
            this.options.showDataOnHover = opt.showDataOnHover;
        }
    }

    redraw() {
        let promises = [];      
        this.calculateMaxChartYAxis();

        if (this.options.showAxisY) {
            let promise = this.redrawYAxis();
            promises.push(promise);
        }

        if (this.options.showAxisX) {
            let promise = this.redrawXAxis();
            promises.push(promise);
        }

        let promise3 = this.redrawSvgLines();
        promises.push(promise3);

        return Promise.all(promises);
    }

    calculateMaxChartXAxis() {
        this.chartMaxXRange = new ChartRange(this.getXAxisMinValue(), this.getXAxisMaxValue());
        this.displayChartXRange = this.chartMaxXRange.clone();
        this.calculateDisplayChartXDataIndexes();
        this.displayChartXIndexesRangePrev = this.displayChartXIndexesRange.clone();
        this.calculateMaxChartYAxis();
    }

    calculateDisplayChartXDataIndexes() {
        let xData = this.getLineData(this.xAxisName);
        let start = 1;
        while (xData[start] < this.displayChartXRange.start) {
            start++;
        }
        if (start - 1 >= 1) {
            start--;
        }

        let end = xData.length - 1;
        while (xData[end] > this.displayChartXRange.end) {
            end--;
        }
        if (end + 1 <= xData.length - 1) {
            end++;
        }

        this.displayChartXIndexesRangePrev = this.displayChartXIndexesRange.clone();
        this.displayChartXIndexesRange = new ChartRange(start, end);
    }

    calculateMaxChartYAxis() {
        this.displayChartYRangePrev = this.displayChartYRange.clone();
        this.displayChartYRange = new ChartRange(this.getYAxisMinValue(), this.getYAxisMaxValue());

        if (this.options.extendAxisYToZero && !this.displayChartYRange.isContain(0)) {
            if (this.displayChartYRange.start > 0) {
                this.displayChartYRange.start = 0;
            }
            else {
                this.displayChartYRange.end = 0;
            }
        }
    }

    draw() {
        this.rootElement.removeChilds();
        this.drawRootSvgElement();
        this.rootElement.appendChild(this.sections.rootSvg)
    }

    drawRootSvgElement() {
        this.sections.rootSvg = document.createSvgElement("svg");
        this.sections.rootSvg.setAttribute("width", this.options.width);
        this.sections.rootSvg.setAttribute("height", this.options.height);
        this.sections.rootSvg.setAttribute("viewBox", "0 0 " + this.options.width + " " + this.options.height);

        if (this.options.showAxisY) {
            this.drawYAxis();
            this.sections.rootSvg.appendChild(this.sections.yAxis);
        }

        if (this.options.showAxisX) {
            this.drawXAxis();
            this.sections.rootSvg.appendChild(this.sections.xAxis);
        }

        this.drawSvgLines();
        this.sections.rootSvg.appendChild(this.sections.linesGroup);


        if (this.options.showDataOnHover) {
            this.showDataOnHover();
            this.sections.rootSvg.appendChild(this.sections.showDataOnHoverElement);
        }

        return this.sections.rootSvg;
    }

    showDataOnHover() {
        let self = this;
        self.sections.showDataOnHoverElement = document.createSvgElement("g");
        self.sections.showDataOnHoverElement.setAttribute("id", "show-data-tooltip");
        this.sections.rootSvg.addEventListener("mousemove", function (event) {
            let relativeXCoord = self.displayChartXRange.start + ((event.offsetX - self.absoluteXRange.start) / self.absoluteXRange.getDifference()) * self.displayChartXRange.getDifference();
            let xData = self.getLineData(self.xAxisName);
            let hoverIndex = -1;
            for (var i = self.displayChartXIndexesRange.start; i < self.displayChartXIndexesRange.end; i++) {
                if (relativeXCoord >= xData[i] && relativeXCoord <= xData[i + 1]) {
                    if (Math.abs(relativeXCoord - xData[i]) < Math.abs(xData[i + 1] - relativeXCoord)) {
                        hoverIndex = i;
                    }
                    else {
                        hoverIndex = i + 1;
                    }
                    break;
                }
            }
            if (hoverIndex > self.displayChartXIndexesRange.start && hoverIndex < self.displayChartXIndexesRange.end) {
                self.showDataTooltip(hoverIndex, xData[hoverIndex]);
            }
        });
        this.sections.rootSvg.addEventListener("mouseout", function (event) {
           self.deleteDataTooltip();
        });
    }

    showDataTooltip(xIndex, xValue) {
        if (this.showingDataTooltipIndex != xIndex) {
            this.showingDataTooltipIndex = xIndex;
            this.deleteDataTooltip();

            let xCoord = this.calculateXCoord(xValue, this.displayChartXRange);
            let tooltipXCoord = xCoord + this.tooltipCircleRadius;
            if (tooltipXCoord + this.showingDataTooltipWidth > this.absoluteXRange.end) {
                tooltipXCoord = xCoord - this.showingDataTooltipWidth - this.tooltipCircleRadius;
            }

            let forObj = document.createSvgElement("foreignObject");
            this.sections.showDataOnHoverElement.appendChild(forObj);
            forObj.setAttribute("class", "node");
            forObj.setAttribute("x", tooltipXCoord);
            forObj.setAttribute("y", this.tooltipCircleRadius);
            forObj.setAttribute("width", this.showingDataTooltipWidth);
            forObj.setAttribute("height", this.absoluteYRange.getDifference());

            let divEl = document.createElement("div");
            forObj.appendChild(divEl);
            divEl.setAttribute("class", "data-tooltip");

            let dateDiv = document.createElement("div");
            divEl.appendChild(dateDiv);
            dateDiv.setAttribute("class", "date");
            dateDiv.innerHTML = new Date(xValue).toShortStringWithDayOfWeek();

            let linesDiv = document.createElement("div");
            divEl.appendChild(linesDiv);
            linesDiv.setAttribute("class", "chart-data");

            for (var i = 0; i < this.lines.length; i++) {
                let lineData = this.getLineData(this.lines[i].name);
                let lineDiv = document.createElement("div");
                linesDiv.appendChild(lineDiv);
                lineDiv.setAttribute("class", "line-data");
                lineDiv.style.cssText = "color: " + this.lines[i].color + ";";
                lineDiv.innerHTML = "<div class='number'>" + lineData[xIndex] + "</div><div class='title'>" + this.lines[i].title + "</div>";

                let verticalLine = document.createSvgElement("path");
                this.sections.showDataOnHoverElement.appendChild(verticalLine);
                verticalLine.setAttribute("opacity", "0.7");
                verticalLine.setAttribute("fill", "none");
                verticalLine.setAttribute("stroke", "#808080");
                verticalLine.setAttribute("d", "M " + xCoord + " 20 V " + (this.absoluteYRange.end).toString());

                let circleEl = document.createSvgElement("circle");
                circleEl.setAttribute("class", "tooltip-label");
                circleEl.setAttribute("opacity", "0.7");
                circleEl.setAttribute("cx", xCoord);
                circleEl.setAttribute("cy", this.calculateYCoord(lineData[xIndex], this.displayChartYRange));
                circleEl.setAttribute("r", this.tooltipCircleRadius);
                circleEl.setAttribute("stroke-width", 3);
                circleEl.setAttribute("stroke", this.lines[i].color);
                circleEl.setAttribute("fill", "none");
                this.sections.showDataOnHoverElement.appendChild(circleEl);
            }
        }
    }

    deleteDataTooltip() {
        this.sections.showDataOnHoverElement.removeChilds();
    }

    drawYAxis() {
        if (this.sections.yAxis == null) {
            this.sections.yAxis = document.createSvgElement("g");
        }
        else {
            this.sections.yAxis.removeChilds();
        }

        this.sections.yAxis.setAttribute("id", "yAxis");
        this.sections.yAxis.setAttribute("opacity", "0.7");

        let rangeYParts = this.divideRangeEqualParts(this.displayChartYRange, this.options.yAxisLinesCount - 1);

        this.drawYAxisNewLines(rangeYParts, this.displayChartXRange, this.displayChartXRange, this.displayChartYRange, this.displayChartYRange);

        return this.sections.yAxis;
    }

    drawYAxisNewLines(rangeYParts, xRange, xRangePrev, yRange, yRangePrev) {
        let promises = [];
        for (let i = 0; i < rangeYParts.length; i++) {
            let horizontalLineEl = document.createSvgElement("path");
            this.sections.yAxis.appendChild(horizontalLineEl);
            horizontalLineEl.setAttribute("fill", "none");
            horizontalLineEl.setAttribute("stroke", "#808080");

            let textEl = document.createSvgElement("text");
            this.sections.yAxis.appendChild(textEl);
            textEl.innerHTML = rangeYParts[i].toString();
            textEl.setAttribute("class", "axis-label");
            textEl.setAttribute("opacity", "0");
            textEl.setAttribute("transform", "translate(0,-5)");

            this.sections.yAxisLinesElements[rangeYParts[i]] = { yRealCoord: rangeYParts[i], horizontalLineEl: horizontalLineEl, textEl: textEl };

            let promise = this.redrawYAxisLine(rangeYParts[i], horizontalLineEl, textEl, xRange, xRangePrev, yRange, yRangePrev, 1, 0);
            promises.push(promise);
        }

        return Promise.all(promises);
    }

    redrawYAxis() {
        let self = this;
        let promises = [];
        let newRangeYParts = this.divideRangeEqualParts(this.displayChartYRange, this.options.yAxisLinesCount - 1);
        for (let prop in this.sections.yAxisLinesElements) {
            if (!this.sections.yAxisLinesElements.hasOwnProperty(prop)) {
                continue;
            }

            let prevYRealCoord = this.sections.yAxisLinesElements[prop].yRealCoord;
            let lineEl = this.sections.yAxisLinesElements[prop].horizontalLineEl;
            let textEl = this.sections.yAxisLinesElements[prop].textEl;
            if (newRangeYParts.indexOf(prevYRealCoord) < 0) {
                let promise = this.redrawYAxisLine(prevYRealCoord, lineEl, textEl, this.displayChartXRange, this.displayChartXRangePrev, this.displayChartYRange, this.displayChartYRangePrev, 0, 1);
                promises.push(promise);
                promise.then(function () {
                    lineEl.remove();
                    textEl.remove();
                    delete self.sections.yAxisLinesElements[prevYRealCoord];
                });
            }
            else {
                let promise = this.redrawYAxisLine(prevYRealCoord, lineEl, textEl, this.displayChartXRange, this.displayChartXRangePrev, this.displayChartYRange, this.displayChartYRangePrev, 1, 1);
                promises.push(promise);
                newRangeYParts.splice(newRangeYParts.indexOf(prevYRealCoord), 1);
            }
        }

        let promise = this.drawYAxisNewLines(newRangeYParts, this.displayChartXRange, this.displayChartXRangePrev, this.displayChartYRange, this.displayChartYRangePrev);
        promises.push(promise);

        return Promise.all(promises);
    }

    redrawYAxisLine(yRealCoord, lineEl, textEl, xRange, xRangePrev, yRange, yRangePrev, opacity, opacityPrev) {
        let self = this;

        return new Promise(function (resolve, reject) {
            let framesCount = self.animateFramesCount;
            let xPrev = xRangePrev.clone();
            let yPrev = yRangePrev.clone();
            let xStartDif = (xRange.start - xPrev.start) / framesCount;
            let xEndDif = (xRange.end - xPrev.end) / framesCount;
            let yStartDif = (yRange.start - yPrev.start) / framesCount;
            let yEndDif = (yRange.end - yPrev.end) / framesCount;
            let opacityDif = (opacity - opacityPrev) / framesCount;

            let currentFrame = 0;
            let rafCallback = function (timestamp) {
                xPrev.start += xStartDif;
                xPrev.end += xEndDif;
                yPrev.start += yStartDif;
                yPrev.end += yEndDif;
                opacityPrev += opacityDif;

                let calcualteYCoord = self.calculateYCoord(yRealCoord, yPrev);
                let calcualteXCoord = self.calculateXCoord(xPrev.start, xPrev)

                let linePathD = "M " + calcualteXCoord + " " + calcualteYCoord + " H " + self.calculateXCoord(xPrev.end, xPrev);
                lineEl.setAttribute("d", linePathD);
                lineEl.setAttribute("opacity", opacityPrev);

                textEl.setAttribute("x", calcualteXCoord);
                textEl.setAttribute("y", calcualteYCoord);
                textEl.setAttribute("opacity", opacityPrev);

                currentFrame++;
                if (currentFrame < framesCount) {
                    requestAnimationFrame(rafCallback);
                }
                else {
                    resolve();
                }
            }
            requestAnimationFrame(rafCallback);
        });
    }

    divideRangeEqualParts(range, partsCount) {
        let partDif = range.getDifference() / partsCount;
        if (partDif == 0) {
            return [range.start];
        }
        let partDifLog10 = Math.trunc(Math.log10(partDif));

        let pow10 = Math.pow(10, partDifLog10);
        let rounedValue;
        if (partDif / pow10 >= 5) {
            rounedValue = pow10;
        }
        else if (partDif / pow10 >= 1.5) {
            rounedValue = pow10 / 2;
        }
        else {
            rounedValue = pow10 / 10;
        }

        partDif = partDif - (partDif % rounedValue);

        let result = [];
        let rangeStart = range.start - (range.start % rounedValue);
        if (rangeStart < range.start) {
            rangeStart += rounedValue;
        }

        for (let i = 0; i < partsCount + 1; i++) {
            let point = rangeStart + i * partDif;
            result.push(point);
        }

        return result;
    }

    drawXAxis() {
        if (this.sections.xAxis == null) {
            this.sections.xAxis = document.createSvgElement("g");
        }
        else {
            this.sections.xAxis.removeChilds();
        }

        this.drawNewXAxis();
    }

    drawNewXAxis() {
        let promises = [];
        let xData = this.getLineData(this.xAxisName);
        let axisLabelsIndexes = this.getDisplayingXAxisLabelsIndexes();

        for (let i = 0; i < axisLabelsIndexes.length; i++) {
            let labelIndex = axisLabelsIndexes[i];
            let labelValue = xData[labelIndex];
            let labelEl = this.createXAxisLabelElement(labelValue);
            this.sections.xAxis.appendChild(labelEl);
            this.sections.xAxisElements[labelIndex] = { index: labelIndex, value: labelValue, labelEl: labelEl };
            let promise = this.redrawXAxisLabel(labelEl, labelValue, this.displayChartXRange, this.displayChartXRange, 1, 0);
            promises.push(promise);
        }

        return Promise.all(promises);
    }

    createXAxisLabelElement(labelValue) {
        let labelEl = document.createSvgElement("text");
        labelEl.innerHTML = new Date(labelValue).toShortString();
        labelEl.setAttribute("class", "axis-label");
        labelEl.setAttribute("text-anchor", "middle");
        labelEl.setAttribute("y", this.absoluteYRange.end);
        labelEl.setAttribute("transform", "translate(0, 16)");
        labelEl.setAttribute("opacity", "0");
        return labelEl;
    }

    getDisplayingXAxisLabelsIndexes() {
        let xData = this.getLineData(this.xAxisName);
        let xDataIndexes = [];
        for (let i = 1; i < xData.length; i++) {
            if (xData[i] >= this.displayChartXRange.start && xData[i] <= this.displayChartXRange.end) {
                xDataIndexes.push(i);
            }
        }

        if (xDataIndexes.length <= this.options.xAxisLabelsCount) {
            return xDataIndexes;
        }

        let result = [];
        let interval = xDataIndexes.length / this.options.xAxisLabelsCount;
        for (let i = 0; i < this.options.xAxisLabelsCount; i++) {
            let ind = Math.round(interval * i + interval / 2);
            result.push(xDataIndexes[ind]);
        }
        return result;
    }

    redrawXAxis() {
        let self = this;
        let promises = [];
        let labelWidth = this.defaultXAxisLabelAbsoluteWidth;
        let halfLabelWidth = this.defaultXAxisLabelAbsoluteWidth / 2;
        let xData = this.getLineData(this.xAxisName);

        let newDisplayingLables = [];
        for (let prop in this.sections.xAxisElements) {
            if (!this.sections.xAxisElements.hasOwnProperty(prop)) {
                continue;
            }
            let xLabel = this.sections.xAxisElements[prop];
            let newCoord = this.calculateXCoord(xLabel.value, this.displayChartXRange);
            // delete that is not in range
            if (newCoord - halfLabelWidth < this.absoluteXRange.start || newCoord + halfLabelWidth > this.absoluteXRange.end) {
                let promise = this.redrawXAxisLabel(xLabel.labelEl, xLabel.value, this.displayChartXRange, this.displayChartXRangePrev, 0, 1);
                promises.push(promise);
                promise.then(function () {
                    xLabel.labelEl.remove();
                });
            }
            else {
                xLabel.newCoord = newCoord;
                newDisplayingLables.push(xLabel);
            }
        }

        // dleete elements between, if they are a lot.
        if (newDisplayingLables.length > this.options.xAxisLabelsCount) {
            for (let i = 1; i < newDisplayingLables.length; i++) {
                let xLabel = newDisplayingLables[i];
                let promise = this.redrawXAxisLabel(xLabel.labelEl, xLabel.value, this.displayChartXRange, this.displayChartXRangePrev, 0, 1);
                promises.push(promise);
                promise.then(function () {
                    xLabel.labelEl.remove();
                });
                newDisplayingLables.splice(i, 1);
            }
        }

        //redraw which remains
        for (let i = 0; i < newDisplayingLables.length; i++) {
            let promise = this.redrawXAxisLabel(newDisplayingLables[i].labelEl, newDisplayingLables[i].value, this.displayChartXRange, this.displayChartXRangePrev, 1, 1);
            promises.push(promise);
        }

        // add element to the left and to the right
        if (newDisplayingLables.length > 0) {
            while (newDisplayingLables[0].newCoord - labelWidth > this.absoluteXRange.start + halfLabelWidth) {
                // create new Elements
                let newXLabelIndex = 0;
                if (newDisplayingLables.length > 1) {
                    newXLabelIndex = newDisplayingLables[0].index - (newDisplayingLables[1].index - newDisplayingLables[0].index);
                }
                else {
                    newXLabelIndex = Math.round(newDisplayingLables[0].index - this.displayChartXIndexesRange.getDifference() / this.options.xAxisLabelsCount);
                }
                if (newXLabelIndex >= 1 && newXLabelIndex < xData.length) {
                    let newXLabelValue = xData[newXLabelIndex];
                    let newLabelEl = self.createXAxisLabelElement(newXLabelValue);
                    this.sections.xAxis.appendChild(newLabelEl);
                    let newCoord = this.calculateXCoord(newXLabelValue, this.displayChartXRange);
                    let newLabel = { index: newXLabelIndex, value: newXLabelValue, labelEl: newLabelEl, newCoord: newCoord };
                    newDisplayingLables.splice(0, 0, newLabel);
                    let promise = this.redrawXAxisLabel(newLabelEl, newXLabelValue, this.displayChartXRange, this.displayChartXRangePrev, 1, 0);
                    promises.push(promise);
                }
                else {
                    break;
                }
            }

            while (newDisplayingLables[newDisplayingLables.length - 1].newCoord + labelWidth < this.absoluteXRange.end - halfLabelWidth) {
                // create new Elements
                let newXLabelIndex = 0;
                if (newDisplayingLables.length > 1) {
                    newXLabelIndex = newDisplayingLables[newDisplayingLables.length - 1].index + (newDisplayingLables[1].index - newDisplayingLables[0].index);
                }
                else {
                    newXLabelIndex = Math.round(newDisplayingLables[newDisplayingLables.length - 1].index + (this.displayChartXIndexesRange.getDifference() / this.options.xAxisLabelsCount));
                }
                if (newXLabelIndex >= 0 && newXLabelIndex < xData.length) {
                    let newXLabelValue = xData[newXLabelIndex];
                    let newLabelEl = self.createXAxisLabelElement(newXLabelValue);
                    this.sections.xAxis.appendChild(newLabelEl);
                    let newCoord = this.calculateXCoord(newXLabelValue, this.displayChartXRange);
                    let newLabel = { index: newXLabelIndex, value: newXLabelValue, labelEl: newLabelEl, newCoord: newCoord };
                    newDisplayingLables.push(newLabel);
                    let promise = this.redrawXAxisLabel(newLabelEl, newXLabelValue, this.displayChartXRange, this.displayChartXRangePrev, 1, 0);
                    promises.push(promise);
                }
                else {
                    break;
                }
            }
        }

        // create new elements between, if they are few
        if (newDisplayingLables.length <= Math.trunc(this.options.xAxisLabelsCount / 2)) {
            for (let i = 1; i < newDisplayingLables.length; i = i + 2) {
                let newXLabelIndex = Math.round((newDisplayingLables[i].index + newDisplayingLables[i - 1].index) / 2);
                let newXLabelValue = xData[newXLabelIndex];
                let newLabelEl = self.createXAxisLabelElement(newXLabelValue);
                this.sections.xAxis.appendChild(newLabelEl);
                let newCoord = this.calculateXCoord(newXLabelValue, this.displayChartXRange);
                let newLabel = { index: newXLabelIndex, value: newXLabelValue, labelEl: newLabelEl, newCoord: newCoord };
                newDisplayingLables.splice(i, 0, newLabel);
                let promise = this.redrawXAxisLabel(newLabelEl, newXLabelValue, this.displayChartXRange, this.displayChartXRangePrev, 1, 0);
                promises.push(promise);
            }
        }

        delete this.sections.xAxisElements;
        this.sections.xAxisElements = {};
        for (let i = 0; i < newDisplayingLables.length; i++) {
            this.sections.xAxisElements[newDisplayingLables[i].index] = newDisplayingLables[i];
        }

        if (newDisplayingLables.length == 0) {
            let promise = this.drawNewXAxis();
            promises.push(promise);
        }

        return Promise.all(promises);
    }

    redrawXAxisLabel(labelEl, xLabelValue, xRange, xRangePrev, opacity, opacityPrev) {
        let self = this;

        return new Promise(function (resolve, reject) {
            let framesCount = self.animateFramesCount;
            let xPrev = xRangePrev.clone();
            let xStartDif = (xRange.start - xPrev.start) / framesCount;
            let xEndDif = (xRange.end - xPrev.end) / framesCount;
            let opacityDif = (opacity - opacityPrev) / framesCount;

            let currentFrame = 0;
            let rafCallback = function (timestamp) {
                xPrev.start += xStartDif;
                xPrev.end += xEndDif;
                opacityPrev += opacityDif;

                let x = self.calculateXCoord(xLabelValue, xPrev);
                labelEl.setAttribute("x", x);
                labelEl.setAttribute("opacity", opacityPrev);

                currentFrame++;
                if (currentFrame < framesCount) {
                    requestAnimationFrame(rafCallback);
                }
                else {
                    resolve();
                }
            }
            requestAnimationFrame(rafCallback);
        });
    }

    redrawSvgLines() {
        let promises = [];
        for (let i = 0; i < this.lines.length; i++) {
            let line = this.lines[i];
            let lineEl = this.sections.linesElements[line.name];

            if (line.visible == true) {
                lineEl.setAttribute("visibility", "visible");
                let promise = this.redrawLineAnimate(line, lineEl, this.displayChartXRange, this.displayChartXRangePrev, this.displayChartYRange, this.displayChartYRangePrev, this.displayChartXIndexesRange, this.displayChartXIndexesRangePrev);
                promises.push(promise);
            }
            else {
                lineEl.setAttribute("visibility", "hidden");
            }
        }
        return Promise.all(promises);
    }

    redrawLineAnimate(line, lineEl, xRange, xRangePrev, yRange, yRangePrev, xIndexesRange, xIndexesRangePrev) {
        let self = this;

        return new Promise(function (resolve, reject) {
            let framesCount = self.animateFramesCount;
            let xPrev = xRangePrev.clone();
            let yPrev = yRangePrev.clone();
            let xIndexPrev = xIndexesRangePrev.clone();
            let xStartDif = (xRange.start - xPrev.start) / framesCount;
            let xEndDif = (xRange.end - xPrev.end) / framesCount;
            let yStartDif = (yRange.start - yPrev.start) / framesCount;
            let yEndDif = (yRange.end - yPrev.end) / framesCount;
            let xIStartDif = (xIndexesRange.start - xIndexesRangePrev.start) / framesCount;
            let xIEndDif = (xIndexesRange.end - xIndexesRangePrev.end) / framesCount;

            let currentFrame = 0;
            let rafCallback = function (timestamp) {
                xPrev.start += xStartDif;
                xPrev.end += xEndDif;
                yPrev.start += yStartDif;
                yPrev.end += yEndDif;
                xIndexPrev.start += xIStartDif;
                xIndexPrev.end += xIEndDif;


                lineEl.setAttribute("d", self.getSvgLinePathD(line.name, xPrev, yPrev, new ChartRange(Math.round(xIndexPrev.start), Math.round(xIndexPrev.end))));

                currentFrame++;
                if (currentFrame < framesCount) {
                    requestAnimationFrame(rafCallback);
                }
                else {
                    resolve();
                }
            }
            requestAnimationFrame(rafCallback);
        });
    }

    drawSvgLines() {
        let self = this;
        this.sections.linesGroup = document.createSvgElement("g");
        this.sections.linesGroup.setAttribute("id", this.rootElementId + "lines-group");
        this.sections.linesGroup.setAttribute("stroke-width", this.getLineStrokeWidth());

        let linesEl = this.drawSvgLineElements();
        linesEl.forEach(function (lineEl) {
            self.sections.linesGroup.appendChild(lineEl);
        });

        return this.sections.linesGroup;
    }

    drawSvgLineElements() {
        let result = [];
        for (let i = 0; i < this.lines.length; i++) {
            if (this.lines[i].visible) {
                let svgLine = this.createSvgLineElement(this.lines[i])
                this.sections.linesElements[this.lines[i].name] = svgLine;
                result.push(svgLine);
            }
        }
        return result;
    }

    createSvgLineElement(line) {
        let pathEl = document.createSvgElement("path");
        pathEl.setAttribute("fill", "none");
        pathEl.setAttribute("opacity", "0.7");
        pathEl.setAttribute("stroke-linejoin", "round");
        pathEl.setAttribute("stroke", line.color);
        pathEl.setAttribute("d", this.getSvgLinePathD(line.name, this.displayChartXRange, this.displayChartYRange, this.displayChartXIndexesRange));
        return pathEl;
    }

    getSvgLinePathD(lineName, displayChartXRange, displayChartYRange, displayChartXIndexesRange) {
        let result = "";
        let xData = this.getLineData(this.xAxisName);
        let lineData = this.getLineData(lineName);
        let i = displayChartXIndexesRange.start;

        // draw first point
        if (i <= displayChartXIndexesRange.end) {
            result += "M " + this.calculateXCoord(xData[i], displayChartXRange) + " " + this.calculateYCoord(lineData[i], displayChartYRange) + " ";
            i++;
        }

        // add points that is before end of display range
        while (i <= displayChartXIndexesRange.end) {
            result += "L " + this.calculateXCoord(xData[i], displayChartXRange) + " " + this.calculateYCoord(lineData[i], displayChartYRange) + " ";
            i++;
        }

        return result;
    }

    calculateXCoord(realXValue, displayRange) {
        let displayRangeDif = displayRange.getDifference();
        if (displayRangeDif == 0) {
            return this.absoluteXRange.start;
        }

        return ((realXValue - displayRange.start) / displayRangeDif) * this.absoluteXRange.getDifference() + this.absoluteXRange.start;
    }

    calculateYCoord(realYValue, displayRange) {
        let displayRangeDif = displayRange.getDifference();
        if (displayRangeDif == 0) {
            return this.absoluteYRange.start;
        }

        let yCoord = ((realYValue - displayRange.start) / displayRangeDif) * this.absoluteYRange.getDifference() + this.absoluteYRange.start;
        return this.invertYAxisValue(yCoord);
    }

    invertYAxisValue(value) {
        return this.options.height - value;
    }

    getBgColor() {
        //return "#242f3e";
        return this.mode == "night" ? "#242f3e" : "#ffffff";
    }

    getXAxisName() {
        let self = this;
        if (!this.initialChartData.types) {
            throw "initialChartData.types is undefined";
        }

        let propName = null;
        this.initialChartData.types.getOwnProperties().forEach(function (prop, i) {
            if (self.initialChartData.types[prop] === "x") {
                propName = prop;
            }
        });

        if (propName == null) {
            throw "initialChartData.types do not contains";
        }

        return propName;
    }

    getLines() {
        let self = this;
        if (!this.initialChartData.types) {
            throw "initialChartData.types is undefined";
        }

        let propNames = [];
        this.initialChartData.types.getOwnProperties().forEach(function (prop, i) {
            if (self.initialChartData.types[prop] === "line") {
                propNames.push(new ChartLine(prop, true, self.getLineTitle(prop), self.getLineColor(prop)));
            }
        });

        return propNames;
    }

    getXAxisMinValue() {
        let xData = this.getLineData(this.xAxisName);
        return xData[1];
    }

    getXAxisMaxValue() {
        let xData = this.getLineData(this.xAxisName);
        return xData[xData.length - 1];
    }

    getYAxisMinValue() {
        let firstVisibleLine = this.getFirstVisibleLine();
        if (firstVisibleLine == null) {
            return 0;
        }

        let min = this.getLineMinValue(firstVisibleLine);
        for (let i = 1; i < this.lines.length; i++) {
            if (this.lines[i].visible === true) {
                let tempMin = this.getLineMinValue(this.lines[i]);
                if (tempMin < min) {
                    min = tempMin;
                }
            }
        }

        return min;
    }

    getLineMinValue(line) {
        let lineData = this.getLineData(line.name);
        if (lineData.length <= 1) {
            return 0;
        }

        let min = lineData[this.displayChartXIndexesRange.start];
        for (let i = this.displayChartXIndexesRange.start + 1; i <= this.displayChartXIndexesRange.end; i++) {
            if (lineData[i] < min) {
                min = lineData[i];
            }
        }

        return min;
    }

    getYAxisMaxValue() {
        let firstVisibleLine = this.getFirstVisibleLine();
        if (firstVisibleLine == null) {
            return 0;
        }

        let max = this.getLineMaxValue(firstVisibleLine);
        for (let i = 1; i < this.lines.length; i++) {
            if (this.lines[i].visible) {
                let tempMax = this.getLineMaxValue(this.lines[i]);
                if (tempMax > max) {
                    max = tempMax;
                }
            }
        }

        return max;
    }

    getLineMaxValue(line) {
        let lineData = this.getLineData(line.name);
        if (lineData.length <= 1) {
            return 0;
        }

        let max = lineData[this.displayChartXIndexesRange.start];
        for (let i = this.displayChartXIndexesRange.start + 1; i <= this.displayChartXIndexesRange.end; i++) {
            if (lineData[i] > max) {
                max = lineData[i];
            }
        }

        return max;
    }

    getFirstVisibleLine() {
        for (let i = 0; i < this.lines.length; i++) {
            if (this.lines[i].visible === true) {
                return this.lines[i];
            }
        }

        return null;
    }

    getLineData(lineName) {
        if (!this.initialChartData.columns) {
            throw "initialChartData.columns is undefined";
        }

        for (let i = 0; i < this.initialChartData.columns.length; i++) {
            if (this.initialChartData.columns[i][0] === lineName)
                return this.initialChartData.columns[i];
        }

        throw "initialChartData.columns doesn't contain " + lineName;
    }

    getLineColor(lineName) {
        if (!this.initialChartData.colors) {
            throw "initialChartData.colors is undefined";
        }

        let color = this.initialChartData.colors[lineName];

        if (!color) {
            throw "initialChartData.colors doesn't contain " + lineName;
        }

        return color;
    }

    getLineTitle(lineName) {
        if (!this.initialChartData.names) {
            throw "initialChartData.names is undefined";
        }

        let name = this.initialChartData.names[lineName];

        return name;
    }

    getLineStrokeWidth() {
        let smallerSide = this.options.height < this.options.width ? this.options.height : this.options.width;

        if (smallerSide <= 10) {
            return 1;
        }

        if (smallerSide <= 100) {
            return 2;
        }

        if (smallerSide <= 1000) {
            return 4;
        }

        return 8;
    }
}

class BaseChartOptions {
    mode;
    showAxisX;
    showAxisY;
    extendAxisYToZero;
    yAxisLinesCount;
    width;
    height;
    showDataOnHover;

    constructor() {
        this.mode = "day";
        this.showAxisX = true;
        this.showAxisY = true;
        this.extendAxisYToZero = false;
        this.yAxisLinesCount = 6;
        this.xAxisLabelsCount = 6;
        this.width = 500;
        this.height = 500;
        this.showDataOnHover = true;
    }
}

class ChartLine {
    name;
    visible;
    title;
    color;

    constructor(name, visible, title, color) {
        this.name = typeof name == "string" ? name : "empty";
        this.visible = typeof visible == "boolean" ? visible : true;
        this.title = typeof title == "string" ? title : name;
        this.color = typeof color == "string" ? color : "#F00";
    }
}

class BaseChartDOMElements {
    rootSvg = document.createElement("div");
    xAxis = document.createElement("div");
    yAxis = document.createElement("div");
    linesGroup = document.createElement("div");
    showDataOnHoverElement = document.createElement("div");


    linesElements = {}
    yAxisLinesElements = {};
    xAxisElements = {};

    constructor() {
        this.rootSvg = null;
        this.xAxis = null;
        this.yAxis = null;
        this.linesGroup = null;
        this.linesElements = {};
        this.yAxisLinesElements = {};
        this.showDataOnHoverElement = null;
    }
}

class ChartRange {
    start = 0;
    end = 0;

    constructor(start, end) {
        if (typeof start != "number") {
            throw "start must be the number.";
        }

        if (typeof end != "number") {
            throw "start must be the number.";
        }

        if (start > end) {
            throw "start must be less than end.";
        }

        this.start = start;
        this.end = end;
    }

    getDifference() {
        return this.end - this.start;
    }

    extend(part) {
        let dif = this.getDifference();
        this.start -= dif * part / 2;
        this.end += dif * part / 2;
    }

    isContain(number) {
        return number >= this.start && number <= this.end;
    }

    clone() {
        return new ChartRange(this.start, this.end);
    }
}

class ChartGroup {
    zoomChartHeight = 70;
    listLinesSectorHeight = 50;
    switchModeSectorHeight = 30;

    rootElementId = "";
    rootElement;
    options = {};

    initialChartData;
    mainChartOptions;

    mainChart;
    zoomChart;
    rangeSelector;

    listLinesSector;
    switchModeSector;
    switchModeTextElement;

    constructor(rootElementId, initialChartData, options) {
        this.rootElementId = rootElementId;
        this.options = options != null ? options : {};
        this.initialChartData = initialChartData;
        this.rootElement = document.getElementById(this.rootElementId);
    }

    draw() {
        let self = this;

        let height = this.options.height != null ? this.options.height : this.rootElement.clientHeight;
        if (height < 250) {
            throw "Chart height is too small.";
        }
        let width = this.options.width != null ? this.options.width : this.rootElement.clientWidth;
        if (width < 250) {
            throw "Chart width is too small.";
        }

        let mainChartId = this.rootElementId + "-main-chart";
        let mainChartHeight = height - this.zoomChartHeight - this.switchModeSectorHeight - this.listLinesSectorHeight;
        let mainChartElement = document.createElement("div");
        mainChartElement.setAttribute("id", mainChartId);
        mainChartElement.style.height = mainChartHeight + "px";
        mainChartElement.style.width = width + "px";
        this.rootElement.appendChild(mainChartElement);

        let zoomChartId = this.rootElementId + "-zoom-chart";
        let rangeChartElement = document.createElement("div");
        rangeChartElement.setAttribute("id", zoomChartId);
        rangeChartElement.style.height = this.zoomChartHeight + "px";
        rangeChartElement.style.width = width + "px";
        this.rootElement.appendChild(rangeChartElement);

        this.zoomChart = new BaseChart(zoomChartId, this.initialChartData, { showAxisY: false, showAxisX: false, extendAxisYToZero: false, showDataOnHover: false, height: this.zoomChartHeight, width: width });
        this.zoomChart.draw();

        let mainChartOptions = this.options != null
            ? this.options
            : { showAxisY: true, showAxisX: true, extendAxisYToZero: false, showDataOnHover: true };
        mainChartOptions.height = mainChartHeight;
        mainChartOptions.width = width;
        this.mainChart = new BaseChart(mainChartId, this.initialChartData, mainChartOptions);
        this.mainChart.draw();

        let initialRangeStart = (this.mainChart.displayChartXRange.start - this.mainChart.chartMaxXRange.start) / this.mainChart.chartMaxXRange.getDifference();
        let initialRangeEnd = (this.mainChart.displayChartXRange.end - this.mainChart.chartMaxXRange.start) / this.mainChart.chartMaxXRange.getDifference();
        let initialRange = new ChartRange(initialRangeStart, initialRangeEnd);

        this.rangeSelector = new RangeSelector(rangeChartElement, initialRange, width);
        this.rangeSelector.onSelectionChange(function (arg) {
            self.mainChart.setDisplayingXAxisRange(arg.relativeRange);
        });
        this.rangeSelector.draw();

        this.addListLinesSector();

        this.addSwitchModeSector();
    }

    addSwitchModeSector() {
        let self = this; 
        this.switchModeSector = document.createElement("div");
        this.switchModeSector.style.cssText = "width: 100%; height: " + this.switchModeSectorHeight + "px;"
            + "display: flex;align-items: flex-end;justify-content: center;";
        this.rootElement.appendChild(this.switchModeSector);

        this.switchModeTextElement = document.createElement("span");
        this.switchModeTextElement.style.cssText = "cursor: pointer; text-align: center; color: #108be3";
        this.switchModeTextElement.innerHTML = "Switch to Night Mode";
        this.switchModeTextElement.addEventListener("click", function (event) {
            self.switchMode();
        });
        this.switchModeSector.appendChild(this.switchModeTextElement);
    }

    addListLinesSector() {
        let self = this;
        this.listLinesSector = document.createElement("div");
        this.listLinesSector.setAttribute("class", "lines-list-sector");
        this.listLinesSector.style.cssText = "height: " + this.listLinesSectorHeight + "px;";
        this.rootElement.appendChild(this.listLinesSector);

        for (let i = 0; i < this.mainChart.lines.length; i++) {
            let line = this.mainChart.lines[i];
            let lineElement = document.createElement("span");
            lineElement.setAttribute("class", "line-select-item line-title");

            let svgCheckbox = document.createSvgElement("svg");
            lineElement.appendChild(svgCheckbox);
            svgCheckbox.setAttribute("width", "25");
            svgCheckbox.setAttribute("height", "20");

            let gEl = document.createSvgElement("g");
            svgCheckbox.appendChild(gEl);
            gEl.setAttribute("stroke", line.color);
            gEl.setAttribute("stroke-width", "2");
            let circleEl = document.createSvgElement("circle");
            gEl.appendChild(circleEl);
            circleEl.setAttribute("cx", "10");
            circleEl.setAttribute("cy", "10");
            circleEl.setAttribute("r", "8.5");
            circleEl.setAttribute("fill-opacity", "0");

            let pathEl = document.createSvgElement("path");
            gEl.appendChild(pathEl);
            pathEl.setAttribute("class", "tick-mark");
            pathEl.setAttribute("d", "M5.2,10 8.5,13.4 14.8,7.2");
            pathEl.setAttribute("fill", "none");

            let textEl = document.createElement("span");
            lineElement.appendChild(textEl);
            textEl.innerHTML = line.title;

            lineElement.addEventListener("click", function (event) {
                if (lineElement.classList.contains("disabled")) {
                    lineElement.classList.remove("disabled");
                    self.mainChart.setLineVisisble(line.name, true);
                    self.zoomChart.setLineVisisble(line.name, true);
                }
                else {
                    lineElement.classList.add("disabled");
                    self.mainChart.setLineVisisble(line.name, false);
                    self.zoomChart.setLineVisisble(line.name, false);
                }
            });
            this.listLinesSector.appendChild(lineElement);
        }
    }

    switchMode() {
        if (this.rootElement.classList.contains("night")) {
            this.rootElement.classList.remove("night");
            this.switchModeTextElement.innerHTML = "Switch to Night Mode";
        }
        else {
            this.rootElement.classList.add("night");
            this.switchModeTextElement.innerHTML = "Switch to Day Mode";
        }
    }
}

class RangeSelector {
    borderWidth = 7;

    rootElement;
    initialRange;
    width;

    leftLayer;
    centerLayer;
    rightLayer;

    leftBorder = document.createElement("div");
    leftBorderPosX = 0;
    rightBorder = document.createElement("div");
    rightBorderPosX = 0;

    dragElementOffsetX;
    dragElementOffsetY;

    onSelectionChangeCallback = null;

    constructor(rootElement, initialRange, width) {
        this.rootElement = rootElement;
        this.initialRange = initialRange;
        this.width = width;
    }

    onSelectionChange(callback) {
        if (typeof callback != "function") {
            throw "callback must be a function.";
        }

        this.onSelectionChangeCallback = callback;
    }

    draw() {
        let self = this;
        this.width = this.width != null ? this.width : this.rootElement.width;

        this.rootElement.style.position = "relative";
        this.rootElement.childNodes.forEach(function (child) {
            child.style.position = "absolute";
        });

        this.leftBorderPosX = this.initialRange.start * this.width;
        this.rightBorderPosX = this.initialRange.end * this.width;

        let parent = document.createElement("div");
        parent.className = "range-selector";
        this.rootElement.appendChild(parent);

        this.leftLayer = document.createElement("div");
        this.leftLayer.className = "layer dark";
        parent.appendChild(this.leftLayer);
        
        this.leftBorder = document.createElement("div");
        this.leftBorder.className = "layer border left";
        this.leftBorder.setAttribute("draggable", "true");
        parent.appendChild(this.leftBorder);

        this.centerLayer = document.createElement("div");
        this.centerLayer.className = "layer";
        this.centerLayer.setAttribute("draggable", "true");
        parent.appendChild(this.centerLayer);

        let centerTopLine = document.createElement("div");
        centerTopLine.className = "layer border line top";
        this.centerLayer.appendChild(centerTopLine);

        let centerBottomLine = document.createElement("div");
        centerBottomLine.className = "layer border line bottom";
        this.centerLayer.appendChild(centerBottomLine);

        this.rightBorder = document.createElement("div");
        this.rightBorder.className = "layer border right";
        this.rightBorder.setAttribute("draggable", "true");
        parent.appendChild(this.rightBorder);

        this.rightLayer = document.createElement("div");
        this.rightLayer.className = "layer dark";
        parent.appendChild(this.rightLayer);

        this.updateElementsPositions();

        let onDragStart = function (event) {
            event.dataTransfer.setData('text/plain', 'anything');
            event.dataTransfer.setDragImage(document.createElement("div"), 0, 0);

            let style = window.getComputedStyle(event.target, null);
            self.dragElementOffsetX = parseInt(style.getPropertyValue("left"), 10) - event.clientX;
            self.dragElementOffsetY = parseInt(style.getPropertyValue("top"), 10) - event.clientY;
        };

        this.rightBorder.addEventListener("dragstart", onDragStart);
        this.leftBorder.addEventListener("dragstart", onDragStart);
        this.centerLayer.addEventListener("dragstart", onDragStart);

        this.rightBorder.addEventListener("drag", function (event) {
            let newElPosX = event.clientX + self.dragElementOffsetX + self.borderWidth;
            if (newElPosX >= 0 && newElPosX <= self.width) {
                if (newElPosX - self.borderWidth > self.leftBorderPosX + self.borderWidth) {
                    //event.target.style.left = newElPosX + "px";
                    self.rightBorderPosX = newElPosX;
                    self.updateElementsPositions();

                    self.selectionChange();
                }
            }
        });

        this.leftBorder.addEventListener("drag", function (event) {
            let newElPosX = event.clientX + self.dragElementOffsetX;
            if (newElPosX >= 0 && newElPosX <= self.width - self.borderWidth) {
                if (newElPosX + self.borderWidth < self.rightBorderPosX - self.borderWidth) {
                    //event.target.style.left = newElPosX + "px";
                    self.leftBorderPosX = newElPosX;
                    self.updateElementsPositions();

                    self.selectionChange();
                }
            }
        });

        this.centerLayer.addEventListener("drag", function (event) {
            let leftBorderPosX = event.clientX + self.dragElementOffsetX - self.borderWidth;
            let rightBorderPosX = self.rightBorderPosX + (leftBorderPosX - self.leftBorderPosX);
            if (leftBorderPosX >= 0 && rightBorderPosX <= self.width) {
                self.leftBorderPosX = leftBorderPosX;
                self.rightBorderPosX = rightBorderPosX;
                self.updateElementsPositions();

                self.selectionChange();
            }
        });
    }

    updateElementsPositions() {
        this.updateElementLeftAndWidth(this.leftLayer, 0, this.leftBorderPosX);
        this.updateElementLeftAndWidth(this.leftBorder, this.leftBorderPosX, this.borderWidth);
        this.updateElementLeftAndWidth(this.centerLayer, this.leftBorderPosX + this.borderWidth, (this.rightBorderPosX - this.leftBorderPosX - this.borderWidth - this.borderWidth));
        this.updateElementLeftAndWidth(this.rightBorder, (this.rightBorderPosX - this.borderWidth), this.borderWidth);
        this.updateElementLeftAndWidth(this.rightLayer, this.rightBorderPosX, (this.width - this.rightBorderPosX));
    }

    updateElementLeftAndWidth(el, left, width) {
        el.style.cssText = "left: " + left + "px;" + "width: " + width + "px;";
    }

    selectionChange() {
        if (this.onSelectionChangeCallback != null) {
            let start = this.leftBorderPosX / this.width;
            let end = this.rightBorderPosX / this.width;
            this.onSelectionChangeCallback({ relativeRange: new ChartRange(start, end) });
        }
    }
}
